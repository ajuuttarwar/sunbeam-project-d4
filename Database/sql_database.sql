/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE DATABASE /*!32312 IF NOT EXISTS*/ `healthbuddy` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `healthbuddy`;
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `appointment`;
CREATE TABLE `appointment` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `user_id` int(100) DEFAULT NULL,
  `doctor_id` int(100) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `phone` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `isApproved` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `doctor_id` (`doctor_id`),
  CONSTRAINT `appointment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `appointment_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `article_id` int(100) NOT NULL AUTO_INCREMENT,
  `writer_id` int(100) DEFAULT NULL,
  `article_title` varchar(100) DEFAULT NULL,
  `article_description` varchar(500) DEFAULT NULL,
  `article_image` varchar(100) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`article_id`),
  KEY `writer_id` (`writer_id`),
  CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`writer_id`) REFERENCES `doctor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `doctor`;
CREATE TABLE `doctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `specialist` varchar(100) DEFAULT NULL,
  `hospital_id` int(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `zip` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '1',
  `image` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `certificate` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hospital_id` (`hospital_id`),
  CONSTRAINT `doctor_ibfk_1` FOREIGN KEY (`hospital_id`) REFERENCES `hospitals` (`hospital_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `hospitals`;
CREATE TABLE `hospitals` (
  `hospital_id` int(100) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(100) DEFAULT NULL,
  `hospital_name` varchar(100) DEFAULT NULL,
  `hospital_address` varchar(100) DEFAULT NULL,
  `hospital_city` varchar(100) DEFAULT NULL,
  `hospital_country` varchar(100) DEFAULT NULL,
  `hospital_zip` varchar(100) DEFAULT NULL,
  `hospital_phone` varchar(20) DEFAULT NULL,
  `hospital_email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`hospital_id`),
  KEY `doctor_id` (`doctor_id`),
  CONSTRAINT `hospitals_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `ngo`;
CREATE TABLE `ngo` (
  `ngo_id` int(100) NOT NULL AUTO_INCREMENT,
  `ngo_name` varchar(100) DEFAULT NULL,
  `ngo_address` varchar(100) DEFAULT NULL,
  `ngo_city` varchar(100) DEFAULT NULL,
  `ngo_country` varchar(100) DEFAULT NULL,
  `ngo_zip` varchar(100) DEFAULT NULL,
  `ngo_phone` varchar(20) DEFAULT NULL,
  `ngo_email` varchar(100) DEFAULT NULL,
  `ngo_description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ngo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `offers`;
CREATE TABLE `offers` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `offer_image` varchar(100) DEFAULT NULL,
  `doctor_id` int(100) DEFAULT NULL,
  `hospital_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doctor_id` (`doctor_id`),
  KEY `hospital_id` (`hospital_id`),
  CONSTRAINT `offers_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`id`),
  CONSTRAINT `offers_ibfk_2` FOREIGN KEY (`hospital_id`) REFERENCES `hospitals` (`hospital_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `zip` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '0',
  `activationToken` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `admin` (`id`,`firstName`,`lastName`,`phone`,`email`,`password`,`createdOn`,`active`) VALUES (1,'ajinkya','uttarwar',NULL,'ajjudac@gmail.com','9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08','2020-09-16 19:00:55',1),(3,'girish','shelke',NULL,'girish@gmail.com','eb070bcd558c431247a91806eb17334b18d9bae30a33962857a66349e00ae2b1','2021-01-14 17:41:06',1),(5,'ajju','ajju',NULL,'ajjudac@gmail.com','dd7e29e290a27b2bed19e91ba7b141c924de91c5c729a3da627d4a16e6ae789e','2021-01-17 03:35:56',1);

INSERT INTO `appointment` (`id`,`user_id`,`doctor_id`,`firstName`,`email`,`createdOn`,`phone`,`date`,`isApproved`) VALUES (4,3,5,'sanket','sanket@gmail.com','2021-01-20 16:29:35','1234567890','2021-01-30',0),(6,12,1,'ajju','girishshelke02@gmail.com','2021-01-22 18:20:21','null','2021-01-30',0),(7,12,6,'ajju','girishshelke02@gmail.com','2021-01-23 00:14:18','null','2021-01-25',1),(8,12,6,'ajju','girishshelke02@gmail.com','2021-01-23 00:14:47','45122545','2021-01-28',1),(9,12,2,'ajju','girishshelke02@gmail.com','2021-01-23 00:24:26','123456789','2021-02-01',0),(10,12,6,'ajju','girishshelke02@gmail.com','2021-01-23 00:34:44','9999999999','2021-01-31',1),(11,12,1,'ajju','girishshelke02@gmail.com','2021-01-23 03:46:41','12457899','2021-01-29',0),(12,12,6,'ajju','girishshelke02@gmail.com','2021-01-23 03:47:49','45788855','2021-01-29',1);

INSERT INTO `articles` (`article_id`,`writer_id`,`article_title`,`article_description`,`article_image`,`createdOn`) VALUES (1,6,'health is wealth','keep good health','0fa4b285e0430ccdabe31480db86eb74','2021-01-23 11:52:44'),(2,6,'healthy life tips','basic tips','e5f3c151d7d62467c3ca3ae263edd3e4','2021-01-23 11:53:27'),(3,6,'Nutrition','knowledge about nutrition','c1cd2a70dac3b61d8dcbfa312acce5f5','2021-01-23 11:54:31'),(4,6,'social media addiction','tips ','ff2def5964b1369be4492d2e1914dc2e','2021-01-23 11:55:29'),(5,6,'brain tumour','tips to survived from brain tumour','ea8331ce5fae9a0de7503a4cdaee1548','2021-01-23 11:56:41');

INSERT INTO `doctor` (`id`,`firstName`,`lastName`,`specialist`,`hospital_id`,`address`,`city`,`country`,`zip`,`phone`,`email`,`password`,`createdOn`,`active`,`image`,`description`,`price`,`rating`,`certificate`) VALUES (1,'Girish','Shelke','Dentist',2,'karve nagar','pune','india','444501','234567856','abc@test.com','abc','2020-09-16 19:45:02',0,'e301b2122a4123f62b6651bd99cef56b','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda perspiciatis deleniti culpa sunt cum, nobis veritatis impedit mollitia cumque, odio vero nemo quae laborum eveniet, pariatur, facilis molestiae minima porro.',300,5,NULL),(2,'efg','efg','Darmatologist',NULL,NULL,'satara',NULL,NULL,'234567856','efg@test.com','efg','2020-09-16 19:46:24',0,NULL,NULL,NULL,NULL,NULL),(3,'xyz','xyz','Homeopath',2,' shashtri chowk','latur','india','444733','982833332','xyz@test.com','xyz','2020-09-16 19:46:24',0,NULL,NULL,NULL,NULL,NULL),(4,'gs','gs','Dentist',8,NULL,'amt',NULL,NULL,'234567856','gs@test.com','gs','2020-09-16 21:38:22',1,NULL,NULL,NULL,NULL,NULL),(5,'girish','shelke','dentist',2,'ashok nagar','nagpur','india','444733','98283848282','girish@gmail.com','d5b93816ea5f613a28bb6223d8ed65bf45f1164475acf21b5426508c27fdeff9','2021-01-21 12:59:11',1,NULL,NULL,300,NULL,'4b32b849adbdbe7dd543f14ca8cf6600'),(6,'girish','shelke','dietitian',7,'gadge nagar','amaravti','india','444709','98283848282','girishshelke02@gmail.com','eb070bcd558c431247a91806eb17334b18d9bae30a33962857a66349e00ae2b1','2021-01-22 22:47:58',1,'c22f326a814c15dedd174b9f05970827',NULL,400,NULL,'892df574cfa423bf98d085ab08da93e5');

INSERT INTO `hospitals` (`hospital_id`,`doctor_id`,`hospital_name`,`hospital_address`,`hospital_city`,`hospital_country`,`hospital_zip`,`hospital_phone`,`hospital_email`) VALUES (2,2,'city hospital','Bardi','Pune','India','411035','12345','mj4@gmail.com'),(7,6,'critical care','pune',NULL,NULL,NULL,'454878787','care@gmail.com'),(8,6,'NueroPoint','Nagar',NULL,NULL,NULL,'7788444','mj4@gmail.com');

INSERT INTO `ngo` (`ngo_id`,`ngo_name`,`ngo_address`,`ngo_city`,`ngo_country`,`ngo_zip`,`ngo_phone`,`ngo_email`,`ngo_description`) VALUES (10,' care','gadnhi','Pune','India','411035','1234567890','care@test.com','one of the best'),(13,'trusty','karve nagar','pune','india','444000','1234578','trusty@gmail.com','best'),(14,'Annamrita Foundation','karve nagar','pune','india','444501','123567890','annamitra@gmail.com','this is for needy people');

INSERT INTO `offers` (`id`,`offer_image`,`doctor_id`,`hospital_id`) VALUES (1,'714da43af72672878a006586826ce591',6,NULL),(2,'30cab10b52c6bd0edb8f5c1825a74bc1',6,NULL),(3,'6b060e2504f99df29531136ae2346dce',6,NULL),(4,'ef7750caf35e937564ce05afd35d0733',6,NULL),(5,'22b881dd52d8c1f7d404185ff84cb283',6,NULL);

INSERT INTO `user` (`id`,`firstName`,`lastName`,`address`,`city`,`country`,`zip`,`phone`,`email`,`password`,`createdOn`,`active`,`activationToken`,`image`) VALUES (1,'girish','shelke','amt','amt','india','444709','928392919281','girish@test.com','eb070bcd558c431247a91806eb17334b18d9bae30a33962857a66349e00ae2b1','2020-09-16 19:13:51',0,NULL,'ef2575f799d0a51ea2150d300dc55322'),(2,'girish','shelke',NULL,NULL,NULL,NULL,NULL,'girish@test.com','eb070bcd558c431247a91806eb17334b18d9bae30a33962857a66349e00ae2b1','2021-01-12 23:49:00',0,NULL,NULL),(3,'sanket','sumant',NULL,'pune','india','444709','123456','sanket@gmail.com','99e7548f3f1e1de8341f47f2c19809597897b54525a568a49ca9d975bae8f38e','2021-01-17 21:03:19',1,NULL,'77fb3fc04a70c786703bb266f872b1a7'),(4,'ajju','ajju',NULL,NULL,NULL,NULL,NULL,'ajju@gmail.com','dd7e29e290a27b2bed19e91ba7b141c924de91c5c729a3da627d4a16e6ae789e','2021-01-18 16:09:50',0,NULL,NULL),(10,'dhiraj','mandade',NULL,NULL,NULL,NULL,NULL,'angulartest6@gmail.com','eb070bcd558c431247a91806eb17334b18d9bae30a33962857a66349e00ae2b1','2021-01-21 02:44:55',0,NULL,NULL),(12,'ajju','uttarwar',NULL,'wardha','india','444709','45789621','girishshelke02@gmail.com','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3','2021-01-22 03:05:03',1,NULL,'ac39a8fecfe0b33bfbc55989b48518ec');

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
