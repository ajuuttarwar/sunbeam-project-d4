
create database healthbuddy;

USE healthbuddy;

---done
create table user (
	id integer PRIMARY KEY auto_increment, 
	firstName VARCHAR(100),
	lastName VARCHAR(100), 
	address VARCHAR(100),
	city VARCHAR(100),
	country VARCHAR(100),
	zip VARCHAR(100),
	phone VARCHAR(20),
	email VARCHAR(100),
	password VARCHAR(100),
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	active INTEGER DEFAULT 0,
	activationToken VARCHAR(100)
);

---donedes
create table doctor (
	id integer PRIMARY KEY auto_increment, 
	firstName VARCHAR(100),
	lastName VARCHAR(100), 
        specialist VARCHAR(100),
        hospital_id INT(100),
	address VARCHAR(100),
	city VARCHAR(100),
	country VARCHAR(100),
	zip VARCHAR(100),
	phone VARCHAR(20),
	email VARCHAR(100),
	password VARCHAR(100),
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	active INTEGER DEFAULT 1,
	FOREIGN KEY(hospital_id) REFERENCES hospitals(hospital_id)
);

---done
create table admin (
	id integer PRIMARY KEY auto_increment, 
	firstName VARCHAR(100),
	lastName VARCHAR(100), 
	phone VARCHAR(20),
	email VARCHAR(100),
	password VARCHAR(100),
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	active INTEGER DEFAULT 1
);

---done
CREATE TABLE hospitals (

    hospital_id INTEGER(10do0) PRIMARY KEY auto_increment,
    doctor_id INTEGER (100)  , 
    hospital_name VARCHAR(100),
    hospital_address VARCHAR(100),
	hospital_city VARCHAR(100),
	hospital_country VARCHAR(100),
	hospital_zip VARCHAR(100),
	hospital_phone VARCHAR(20),
	hospital_email VARCHAR(100),
	FOREIGN KEY(doctor_id) REFERENCES doctor(id)
   
);

---done
CREATE TABLE ngo(

    ngo_id INTEGER(100) PRIMARY KEY auto_increment,
    ngo_name VARCHAR(100),
    ngo_address VARCHAR(100),
	ngo_city VARCHAR(100),
	ngo_country VARCHAR(100),
	ngo_zip VARCHAR(100),
	ngo_phone VARCHAR(20),
	ngo_email VARCHAR(100),
	ngo_description VARCHAR(100)

);


--done
CREATE TABLE articles  (

    article_id INT(100) PRIMARY KEY auto_increment,
    writer_id INT (100) ,
    article_title VARCHAR(100),
    article_description VARCHAR(200),
    article_image VARCHAR(100),
    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY(writer_id) REFERENCES doctor(id)
);


--done
CREATE TABLE offers(

    id INTEGER(100) PRIMARY KEY auto_increment,
    offer_image VARCHAR(100),
    doctor_id INTEGER(100),
    hospital_id INTEGER(100) ,
FOREIGN KEY(doctor_id) REFERENCES doctor(id),
FOREIGN KEY(hospital_id) REFERENCES hospitals(hospital_id)


);


Create Table appointment(
	id INTEGER(100) PRIMARY KEY auto_increment,
	user_id INTEGER(100),
	doctor_id INTEGER(100),
	firstName VARCHAR(100),
	email VARCHAR(100),
 createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	phone VARCHAR(100),
	date VARCHAR(100),
	isApproved INTEGER DEFAULT 0,
FOREIGN KEY(user_id) REFERENCES user(id),
FOREIGN KEY(doctor_id) REFERENCES doctor(id)

);




