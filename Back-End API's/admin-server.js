const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const jwt = require('jsonwebtoken')
const config = require('./config')


// routers
const adminRouter = require('./admin/routes/admin')
const articlesRouter = require('./admin/routes/articles')
const hospitalRouter = require('./admin/routes/hospital')
const ngoRouter = require('./admin/routes/ngo')


const app = express()
app.use(bodyParser.json())
app.use(morgan('combined'))





// add a middleware for getting the id from token
function getUserId(request, response, next) {

    if (request.url == '/admin/signin' || request.url == '/admin/signup') {
      // do not check for token 
      next()
    } else {
  
      try {
        const token = request.headers['token']
        const data = jwt.verify(token, config.secret)
  
        // add a new key named userId with logged in user's id
        request.userId = data['id']
  
        // go to the actual route
        next()
        
      } catch (ex) {
        response.status(401)
        response.send({status: 'error', error: 'protected api'})
      }
    }
  }



app.use(getUserId)

// add the routes
app.use('/admin', adminRouter)
app.use('/articles', articlesRouter)
app.use('/hospital', hospitalRouter)
app.use('/ngo', ngoRouter)



// default route
app.get('/', (request, response) => {
    response.send('welcome to my application')
  })
  
  app.listen(4000, '0.0.0.0', () => {
    console.log('server started on port 4000')
  })
