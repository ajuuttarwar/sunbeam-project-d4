const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const jwt = require('jsonwebtoken')
const config = require('./config')


// routers
const userRouter = require('./user/routes/user')



const app = express()
app.use(bodyParser.json())
app.use(morgan('combined'))





function getUserId(request, response, next) {

    if (request.url == '/user/signin' || request.url == '/user/signup') {

        next()
    } else {
  
      try {
        const token = request.headers['token']
        const data = jwt.verify(token, config.secret)
  
        request.userId = data['id']
  
        next()
        
      } catch (ex) {
        response.status(401)
        response.send({status: 'error', error: 'protected api'})
      }
    }
  }



app.use(getUserId)

// add the routes
app.use('/user', userRouter)





// default route
app.get('/', (request, response) => {
    response.send('welcome to my user application')
  })
  
  app.listen(5000, '0.0.0.0', () => {
    console.log('server started on port 5000')
  })