const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const crypto = require('crypto-js')

const router = express.Router()



// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/details/:doctor_id', (request, response) => {
    const { doctor_id } = request.params
    const statement = `select hospital_id, hospital_name, hospital_address, hospital_email, hospital_phone from hospitals where doctor_id = ${doctor_id}`
    db.query(statement, (error, hospitals) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (hospitals.length == 0) {
                response.send({ status: 'error', error: 'Hospital does not exist' })
            } else {
                const hospital = hospitals[0]
                response.send(utils.createResult(error, hospital))
            }
        }
    })
})
// ----------------------------------------------------
// POST
// ----------------------------------------------------

router.post('/add', (request, response) => {
    const { doctor_id, hospital_name, hospital_address, hospital_phone, hospital_email } = request.body
    const statement = `insert into hospitals ( doctor_id, hospital_name, hospital_address, hospital_phone, hospital_email) values (
    '${doctor_id}', '${hospital_name}','${hospital_address}', '${hospital_phone}','${hospital_email}')`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})
// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put('/edit/:hospital_id', (request, response) => {
    const { hospital_id } = request.params
    const { hospital_name, hospital_address, hospital_phone, hospital_email } = request.body
    const statement = `UPDATE hospitals set hospital_name ='${hospital_name}', hospital_address= '${hospital_address}', hospital_phone= '${hospital_phone}', hospital_email= '${hospital_email}' where hospital_id = '${hospital_id}'`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})
// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('/delete/:hospital_id', (request, response) => {
    const { hospital_id } = request.params
    const statement = `delete from hospitals where hospital_id = ${hospital_id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router