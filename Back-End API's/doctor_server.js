const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const jwt = require('jsonwebtoken')
const config = require('./config')


// routers
const doctorRouter = require('./doctor/routes/doctor')
const hospitalRouter = require('./doctor/routes/hospital')




const app = express()
app.use(bodyParser.json())
app.use(morgan('combined'))





// add a middleware for getting the id from token
function getUserId(request, response, next) {

  if (request.url == '/doctor/signin'
    || request.url == '/doctor/signup'
    || request.url == '/certificate.jpg') {
    // do not check for token 
    next()
  } else {

    try {
      const token = request.headers['token']
      const data = jwt.verify(token, config.secret)

      // add a new key named userId with logged in user's id
      request.userId = data['id']

      // go to the actual route
      next()

    } catch (ex) {
      response.status(401)
      response.send({ status: 'error', error: 'protected api' })
    }
  }
}



app.use(getUserId)
app.use(express.static('images/'))


// add the routes
app.use('/doctor', doctorRouter)
app.use('/hospital', hospitalRouter)



// default route
app.get('/', (request, response) => {
  response.send('welcome to my application')
})

app.listen(5000, '0.0.0.0', () => {
  console.log('server started on port 5000')
})