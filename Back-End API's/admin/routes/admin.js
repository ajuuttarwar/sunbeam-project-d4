const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')

const router = express.Router()


// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/profile', (request, response) => {
  const statement = `select firstName, lastName, email, phone from admin where id = ${request.userId}`
  db.query(statement, (error, admins) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (admins.length == 0) {
        response.send({ status: 'error', error: 'admin does not exist' })
      } else {
        const admin = admins[0]
        response.send(utils.createResult(error, admin))
      }
    }
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------
//signup

router.post('/signup', (request, response) => {
  const { firstName, lastName, email, password } = request.body

  const encryptedPassword = crypto.SHA256(password)
  const statement = `insert into admin (firstName, lastName, email, password) values (
    '${firstName}', '${lastName}', '${email}', '${encryptedPassword}'
  )`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})




//signin-post

router.post('/signin', (request, response) => {
  const { email, password } = request.body
  const statement = `select id, firstName, lastName from admin where email = '${email}' and password = '${crypto.SHA256(password)}'`
  db.query(statement, (error, admins) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (admins.length == 0) {
        response.send({ status: 'error', error: 'admin does not exist' })
      } else {
        const admin = admins[0]
        const token = jwt.sign({ id: admin['id'] }, config.secret)
        response.send(utils.createResult(error, {
          firstName: admin['firstName'],
          lastName: admin['lastName'],
          token: token
        }))
      }
    }
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// PUT
// ----------------------------------------------------

// router.put('/:id', (request, response) => {
//   const { id } = request.params

//   const { firstName, lastName, email, password } = request.body

//   const encryptedPassword = crypto.SHA256(password)
//   const statement = `UPDATE admin set firstName=${firstName}, lastName= ${lastName}, email= ${email}, password= ${password} where admin_id = ${id}'
//   )`


//   db.query(statement, (error, data) => {

//     response.send(utils.createResult(error, data))
//   })
// })
// // ----------------------------------------------------



// // ----------------------------------------------------
// // DELETE
// // ----------------------------------------------------

// router.delete('/:id', (request, response) => {
//   const { id } = request.params
//   const statement = `delete from admin where id = ${id}`
//   db.query(statement, (error, data) => {
//     response.send(utils.createResult(error, data))
//   })
// })


// ----------------------------------------------------

module.exports = router