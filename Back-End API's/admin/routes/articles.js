const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const { request } = require('express')
const router = express.Router()


//POST-add new article into the database table
router.post('/', (request, response)=>
{

    const{ article_title, article_description}= request.body

   const statement=`insert into articles(article_title,article_description) values('${article_title}','${article_description}')`

    db.query(statement, (error,data)=>{

        response.send(utils.createResult(error,data))
    })
})


//GET- Get all the articles
router.get('/', (request, response)=>
{
    const statement = `select * from articles`

    db.query(statement, (error,data)=>{

        response.send(utils.createResult(error,data))
    })
})





//PUT-update existing article
router.put('/:id', (request, response)=>
{
    const {id} = request.params

    const{ article_title, article__description}= request.body

    const statement = `UPDATE articles set article_title=${article_title}, article_description= ${article__description} where article_id = ${id}`



    db.query(statement, (error,data)=>{

        response.send(utils.createResult(error,data))
    })
})


//Delete existing article
router.delete('/:id', (request, response) => {
    const {id} = request.params
    const statement = `delete from articles where article_id = ${id}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })




module.exports = router