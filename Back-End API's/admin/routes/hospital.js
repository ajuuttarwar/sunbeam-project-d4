const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const router = express.Router()



//GET- Get all the HOSPITAL
router.get('/', (request, response)=>
{

    
    const statement = `select * from hospitals`

    db.query(statement, (error,data)=>{

        response.send(utils.createResult(error,data))
    })
})



//POST-add new HOSPITAL into the database table
router.post('/', (request, response)=>
{

    const{ doctor_id , hospital_name , hospital_address , hospital_city , hospital_country , hospital_zip , hospital_phone, hospital_email  }= request.body

    const statement = `INSERT INTO hospitals(doctor_id , hospital_name , hospital_address , hospital_city , hospital_country , hospital_zip , hospital_phone, hospital_email )
     values ( ${doctor_id}, '${hospital_name}' , '${hospital_address}' , '${hospital_city}' , '${hospital_country}', '${hospital_zip}', '${hospital_phone}', '${hospital_email}')`



    db.query(statement, (error,data)=>{

        response.send(utils.createResult(error,data))
    })
})






//PUT-update existing HOSPITAL
router.put('/:id', (request, response)=>
{
    const {id} = request.params

    const{ doctor_id , hospital_name , hospital_address , hospital_city , hospital_country , hospital_zip , hospital_phone, hospital_email }= request.body

    const statement = `UPDATE hospitals SET doctor_id=${doctor_id}, hospital_name='${hospital_name}', hospital_address='${hospital_address}', hospital_city='${hospital_city}', hospital_country='${hospital_country}', hospital_zip='${hospital_zip}', hospital_phone='${hospital_phone}', hospital_email='${hospital_email}' WHERE hospital_id = ${id}`



    db.query(statement, (error,data)=>{

        response.send(utils.createResult(error,data))
    })
})


//Delete existing HOSPITAL
router.delete('/:id', (request, response) => {
    const {id} = request.params
    const statement = `DELETE FROM hospitals WHERE hospital_id = ${id}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })



module.exports = router