const nodemailer = require('nodemailer')
const config = require('./config');
const utils = require('./utils');

function sendEmail(email, subject, body, callback) {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: config.userEmail,
            pass: config.userPassword
        }
    })

    const mailOptions = {
        from: config.userEmail,
        to: email,
        subject: subject,
        html: body
    }

    transporter.sendMail(mailOptions, callback)
}

module.exports = {
    sendEmail : sendEmail
}