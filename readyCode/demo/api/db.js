const mysql = require('mysql2')

const pool = mysql.createPool({
  host: 'localhost',
  user: 'hb',
  database: 'healthbuddy',
  password: 'hb',
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
})


module.exports = pool