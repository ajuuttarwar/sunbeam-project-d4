const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const router = express.Router()




//POST-add new NGO into the database table
router.post('/create', (request, response) => {

    const { ngo_name, ngo_address, ngo_city, ngo_country, ngo_zip, ngo_phone, ngo_email, ngo_description } = request.body

    const statement = `insert into ngo(ngo_name,  ngo_address, ngo_city, ngo_country, ngo_zip, ngo_phone, ngo_email,ngo_description) 
                         values('${ngo_name}','${ngo_address}', '${ngo_city}','${ngo_country}','${ngo_zip}', '${ngo_phone}', '${ngo_email}','${ngo_description}')`


    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })
})


//GET- Get all the NGO
router.get('/', (request, response) => {


    const statement = `select ngo_id,ngo_name,ngo_address,ngo_city,ngo_country,ngo_phone,ngo_email,ngo_description from ngo`

    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })
})


router.get('/:id', (request, response) => {

    const { id } = request.params

    const statement = `select ngo_id,ngo_name,ngo_address,ngo_city,ngo_country,ngo_phone,ngo_email,ngo_description from ngo where ngo_id=${id}`

    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })
})



//PUT-update existing NGO
router.put('/:id', (request, response) => {
    const { id } = request.params


    const { ngo_name, ngo_address, ngo_city, ngo_country, ngo_zip, ngo_phone, ngo_email, ngo_description } = request.body

    const statement = `UPDATE ngo SET ngo_name='${ngo_name}', ngo_address='${ngo_address}', ngo_city='${ngo_city}', ngo_country='${ngo_country}', ngo_zip='${ngo_zip}', ngo_phone='${ngo_phone}', ngo_email='${ngo_email}', ngo_description='${ngo_description}' WHERE ngo_id = ${id}`



    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })
})


//Delete existing NGO
router.delete('/:id', (request, response) => {
    const { id } = request.params
    const statement = `DELETE FROM ngo WHERE ngo_id =${id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})





module.exports = router