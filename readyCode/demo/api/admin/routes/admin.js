const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')

const router = express.Router()


// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/profile', (request, response) => {
  const statement = `select firstName, lastName, email, phone from admin where id = ${request.userId}`
  db.query(statement, (error, admins) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (admins.length == 0) {
        response.send({ status: 'error', error: 'admin does not exist' })
      } else {
        const admin = admins[0]
        response.send(utils.createResult(error, admin))
      }
    }
  })
})



router.put('/toggle-doctor/:id', (request, response) => {
  const {id} = request.params
  const {status} = request.body
  console.log(id)
  const statement = `update doctor set active = ${status} where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})



router.get('/countDoctor', (request, response) => {
  const statement = `select count(id) from doctor`
  db.query(statement, (error, datas) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
     
        const data = datas[0]
        response.send(utils.createResult(error, data))
     
    }
  })
})

router.get('/allDoctor', (request, response)=>
{

  // select d.id,d.firstName,d.lastName,d.specialist,h.hospital_name,d.country,d.phone,d.email,d.active from doctor d inner join hospitals h ON d.hospital_id=h.hospital_id;


  const statement = `select d.id,d.firstName,d.lastName,d.specialist,h.hospital_name,d.country,d.phone,d.email,d.active from doctor d inner join hospitals h ON d.hospital_id=h.hospital_id`
  

    db.query(statement, (error,data)=>{

        response.send(utils.createResult(error,data))
    })
})







// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------
//signup

router.post('/signup', (request, response) => {
  const { firstName, lastName, email, password } = request.body

  const encryptedPassword = crypto.SHA256(password)
  const statement = `insert into admin (firstName, lastName, email, password) values (
    '${firstName}', '${lastName}', '${email}', '${encryptedPassword}'
  )`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})




//signin-post

router.post('/signin', (request, response) => {
  const { email, password } = request.body
  const statement = `select id, firstName, lastName from admin where email = '${email}' and password = '${crypto.SHA256(password)}'`
  db.query(statement, (error, admins) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (admins.length == 0) {
        response.send({ status: 'error', error: 'admin does not exist' })
      } else {
        const admin = admins[0]
        const token = jwt.sign({ id: admin['id'] }, config.secret)
        response.send(utils.createResult(error, {
          firstName: admin['firstName'],
          lastName: admin['lastName'],
          token: token
        }))
      }
    }
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// PUT
// ----------------------------------------------------

// router.put('/:id', (request, response) => {
//   const { id } = request.params

//   const { firstName, lastName, email, password } = request.body

//   const encryptedPassword = crypto.SHA256(password)
//   const statement = `UPDATE admin set firstName=${firstName}, lastName= ${lastName}, email= ${email}, password= ${password} where admin_id = ${id}'
//   )`


//   db.query(statement, (error, data) => {

//     response.send(utils.createResult(error, data))
//   })
// })
// // ----------------------------------------------------



// // ----------------------------------------------------
// // DELETE
// // ----------------------------------------------------

// router.delete('/:id', (request, response) => {
//   const { id } = request.params
//   const statement = `delete from admin where id = ${id}`
//   db.query(statement, (error, data) => {
//     response.send(utils.createResult(error, data))
//   })
// })


// ----------------------------------------------------

module.exports = router