const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const { request } = require('express')
const router = express.Router()


//POST-add new article into the database table
router.post('/', (request, response)=>
{

    const{ writer_id,article_title, article_description}= request.body

   const statement=`insert into articles(writer_id,article_title,article_description) values('${writer_id}','${article_title}','${article_description}')`

    db.query(statement, (error,data)=>{

        response.send(utils.createResult(error,data))
    })
})


//GET- Get all the articles
router.get('/', (request, response)=>
{
    // select a.article_id,h.firstName,h.lastName,a.article_title,a.article_description,a.article_image,a.createdOn from articles a inner join doctor h where a.writer_id = h.id;

    const statement = `select a.article_id,d.firstName,d.lastName,a.article_title,a.article_description,a.article_image,a.createdOn from articles a inner join doctor d where a.writer_id = d.id`

    db.query(statement, (error,data)=>{

        response.send(utils.createResult(error,data))
    })
})


router.get('/details/:id', (request, response)=>
{
    const {id} = request.params
    const statement = `select a.article_id,h.firstName,h.lastName,a.article_title,a.article_description,a.article_image,a.createdOn from articles a inner join doctor h where a.writer_id = h.id having a.article_id=${id};`

    db.query(statement, (error,data)=>{

        response.send(utils.createResult(error,data))
    })
})






//PUT-update existing article
router.put('/:id', (request, response)=>
{
    const {id} = request.params

    const{ article_title, article_description}= request.body
    
    // console.log(id+article_description+article_title)

    const statement = `UPDATE articles set article_title= '${article_title}', article_description= '${article_description}' where article_id = ${id}`

    db.query(statement, (error,data)=>{

        response.send(utils.createResult(error,data))
    })
})


//Delete existing article
router.delete('/:id', (request, response) => {
    const {id} = request.params
    const statement = `delete from articles where article_id = ${id}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })




module.exports = router