const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const { request } = require('express')
const router = express.Router()



//GET- Get all the articles
router.get('/allPatient', (request, response)=>
{
    const statement = `select id,firstName,lastName,address, city,country,phone,email,active from user`

    db.query(statement, (error,data)=>{

        response.send(utils.createResult(error,data))
    })
})




router.put('/toggle-patient/:id', (request, response) => {
  const {id} = request.params
  const {status} = request.body
  console.log(id)
  const statement = `update user set active = ${status} where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})


//Delete existing article
router.delete('/:id', (request, response) => {
    const {id} = request.params
    const statement = `delete from articles where article_id = ${id}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })




module.exports = router