const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const fs = require('fs')
const mailer = require('../../mailer')
const uuid = require('uuid')
const path = require('path')
const multer = require('multer')
const upload = multer({ dest: 'images/' })


const router = express.Router()



//get his profile by email
router.post('/profile-by-email', (request, response) => {
  const {email} = request.body
  const statement = `select * from user where email = '${email}'`
  db.query(statement, (error, users) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (users.length == 0) {
        response.send({status: 'error', error: 'user does not exist'})
      } else {
        const user = users[0]

        response.send(utils.createResult(error, user))
      }
    }
  })
})



//Forgot Password functionality

router.post('/forgot-password', (request, response) => {
  const {email} = request.body
  const statement = `select id, firstName, lastName from user where email = '${email}'`
  db.query(statement, (error, users) => {
    if (error) {
      response.send(utils.createError(error))
    } else if (users.length == 0) {
      response.send(utils.createError('user does not exist'))
    } else {
      const user = users[0]
      const otp = utils.generateOTP()
      const body = `Your OTP is <h1>${otp}</h1>` 

      mailer.sendEmail(email, 'Reset your password', body,  (error, info) => {
        response.send(
          utils.createResult(error, {
            otp: otp,
            email: email
          })
        )
      })
    }
  })
})


router.put('/reset-after-forgot', (request, response) => {
  const { email, password } = request.body

  const statement = `update user set password = '${crypto.SHA256(password)}' where email = '${email}'`

  db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
  })
})














router.put('/reset-password', (request, response) => {
  const { password } = request.body

  const statement = `update user set password = '${crypto.SHA256(password)}' where id = ${request.userId}`

  db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
  })
})



// -----------------------------------------------
// appointment functionality
// --------------------------------------------


router.post('/book-appointment/:d_id', (request, response) => {
  const { d_id } = request.params
  // const{id}=request.userId
  const { name, email, phone, date } = request.body
  const statement = `insert into appointment(user_id,doctor_id,firstName,email,phone,date) values(${request.userId},${d_id},'${name}','${email}','${phone}','${date}')`
  console.log(d_id)
  console.log(request.userId)
  console.log(name)
  console.log(email)
  console.log(phone)
  console.log(date)

  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      console.log('success')
      response.send(utils.createResult(error, data))

    }
  })
})


// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/getCity', (request, response) => {
  const statement = `select  city from doctor `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})

router.get('/getSpecialist', (request, response) => {
  const statement = `select distinct specialist from doctor `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})




router.get('/getDoctorAppointmentDetails', (request, response) => {
  // const statement = `select  * from doctor d inner join appointment a on d.id=a.doctor_id inner join hospitals h on d.hospital_id=h.hospital_id `
 
  const statement = `select  * from doctor d inner join appointment a on d.id=a.doctor_id where a.user_id=${request.userId}`

  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})




router.get('/image/:filename', (request, response) => {
  const { filename } = request.params
  console.log(__dirname)
  console.log('hi')
  const file = fs.readFileSync(__dirname + '/../../images/' + filename)
  response.send(file)
})



// router.get('/image/:filename', (request, response) => {
//   const { filename } = request.params
//   console.log(__dirname)
//   console.log('hi')
//   const file = fs.readFileSync(__dirname + '/../../images/' + filename)
//   response.send(file)
// })



router.get('/get-all-articles', (request, response) => {
  const statement = `select a.article_title,a.article_description,a.article_image,d.firstName,d.lastName from articles a inner join doctor d on a.writer_id=d.id `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})




router.get('/get-all-ngo', (request, response) => {
  const statement = `select *  from ngo `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})


router.get('/get-ngo-by-city', (request, response) => {

  const { ngo_city } = request.body
  const statement = `select *  from ngo where ngo_city='${ngo_city}' `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})


router.get('/get-doctor-by-id/:id', (request, response) => {
  const { id } = request.params
  // const statement = `select d.id,d.firstName,d.lastName,d.image,d.price,d.rating,d.specialist,h.hospital_name,h.hospital_id,d.address,d.description,d.city,d.country,d.zip,d.phone,d.email,d.password from doctor d inner join hospitals h on d.hospital_id=h.hospital_id having id=${id}`
  
  const statement = `select id,firstName,lastName,image,price,rating,specialist,address,description,city,country,zip,phone,email,password from doctor where id=${id}`
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})



router.get('/get-all-doctors', (request, response) => {
  const statement = `select * from doctor `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})


router.post('/get-doctor-by-field', (request, response) => {
  const { specialist } = request.body
  const statement = `select d.id,d.firstName,d.lastName,d.specialist,d.image,h.hospital_name,d.address,d.city,d.country,d.phone,d.email from doctor d inner join hospitals h on d.hospital_id=h.hospital_id having d.specialist='${specialist}'`
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})

router.get('/get-all-hospital', (request, response) => {
  const statement = `select * from hospitals `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})

router.get('/get-hospital-by-id/:id', (request, response) => {
  const { id } = request.params
  const statement = `select * from hospitals where hospital_id=${id} `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})


router.get('/get-ngo-by-id/:id', (request, response) => {
  const { id } = request.params
  const statement = `select * from ngo where ngo_id=${id} `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})

router.post('/get-doctor-by-city-specialist', (request, response) => {

  const { specialist, city } = request.body
  const statement = `select d.id,d.firstName,d.lastName,d.image,d.specialist,h.hospital_name,d.address,d.city,d.country,d.phone,d.email from doctor d inner join hospitals h on d.hospital_id=h.hospital_id having d.specialist='${specialist}' and d.city='${city}'`
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})





router.get('/profile', (request, response) => {
  const statement = `select * from user where id = ${request.userId}`
  db.query(statement, (error, users) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (users.length == 0) {
        response.send({ status: 'error', error: 'user does not exist' })
      } else {
        const user = users[0]
        response.send(utils.createResult(error, user))
      }
    }
  })
})




router.get('/activate/:token', (request, response) => {
  const { token } = request.params

  // activate the customer
  // reset the activation token
  const statement = `update user set active = 1, activationToken = '' where activationToken = '${token}'`
  db.query(statement, (error, data) => {

    console.log('it works')
    const htmlPath = path.join(__dirname, './../../user-email-templates/activation_result.html')
    const body = '' + fs.readFileSync(htmlPath)
    response.header('Content-Type', 'text/html')
    response.send(body)
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------
//signup

router.post('/signup', (request, response) => {
  const { firstName, lastName, email, password } = request.body



  const activationToken = uuid.v4()
  const activationLink = `http://localhost:5000/user/activate/${activationToken}`

  console.log(__dirname)
  const htmlPath = path.join(__dirname, './../../user-email-templates/send_activation_link.html')

  let body = '' + fs.readFileSync(htmlPath)
  body = body.replace('firstName', firstName)
  body = body.replace('activationLink', activationLink)


  const encryptedPassword = crypto.SHA256(password)
  const statement = `insert into user (firstName, lastName, email, password) values (
    '${firstName}', '${lastName}', '${email}', '${encryptedPassword}'
  )`
  db.query(statement, (error, data) => {
    mailer.sendEmail(email, 'Welcome to HeathBuddy', body, (error, info) => {
      console.log(error)
      console.log(info)
      response.send(utils.createResult(error, data))
    })
  })
})






  //upload image 
  router.post('/upload-image', upload.single('image'), (request, response) => {
    const fileName = request.file.filename

    const statement = `update user set image = '${fileName}' where id = ${request.userId}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })


  // doctor image upload

  router.post('/upload-image/:id', upload.single('image'), (request, response) => {
    const fileName = request.file.filename
    const { id } = request.params

    const statement = `update doctor set image = '${fileName}' where id = ${id}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })




  //signin-post

  // router.post('/signin', (request, response) => {
  //   const { email, password } = request.body
  //   const statement = `select id, firstName, lastName from user where email = '${email}' and password = '${crypto.SHA256(password)}'`
  //   db.query(statement, (error, users) => {
  //     if (error) {
  //       response.send({ status: 'error', error: error })
  //     } else {
  //       if (users.length == 0) {
  //         response.send({ status: 'error', error: 'user does not exist' })
  //       } else {
  //         const user = users[0]
  //         const token = jwt.sign({ id: user['id'] }, config.secret)
  //         response.send(utils.createResult(error, {
  //           firstName: user['firstName'],
  //           lastName: user['lastName'],
  //           token: token
  //         }))
  //       }
  //     }
  //   })
  // })



  router.post('/signin', (request, response) => {
    const { email, password } = request.body
    const statement = `select id, firstName, lastName, active from user where email = '${email}' and password = '${crypto.SHA256(password)}'`
    db.query(statement, (error, users) => {
      if (error) {
        response.send({ status: 'error', error: error })
      } else if (users.length == 0) {
        response.send({ status: 'error', error: 'users does not exist' })
      } else {
        const user = users[0]
        if (user['active'] == 1) {
          // vendor is an active vendor
          const token = jwt.sign({ id: user['id'] }, config.secret)
          response.send(utils.createResult(error, {
            firstName: user['firstName'],
            lastName: user['lastName'],
            token: token
          }))
        } else {
          // vendor is a suspended vendor
          response.send({ status: 'error', error: 'your account is not active. please contact administrator' })
        }
      }
    })
  })

  // ----------------------------------------------------



  // ----------------------------------------------------
  // PUT
  // ----------------------------------------------------

  router.put('/edit-user', (request, response) => {
    // const { id } = request.params


    const { firstName, lastName, city, country, zip, phone } = request.body

    // const encryptedPassword = crypto.SHA256(password)
    const statement = `UPDATE user set firstName='${firstName}', lastName= '${lastName}',city='${city}',country='${country}',zip='${zip}',phone='${phone}' where id = ${request.userId}`


    db.query(statement, (error, data) => {

      response.send(utils.createResult(error, data))
    })
  })


  router.put('/edit-doctor/:id', (request, response) => {
    const { id } = request.params

    const { firstName, lastName, specialist, hospital_name, hospital_id, address, city, country, zip, phone, email, password } = request.body

    // const encryptedPassword = crypto.SHA256(password)
    const statement = `UPDATE doctor set firstName='${firstName}', lastName= '${lastName}',specialist='${specialist}',address='${address}',city='${city}',country='${country}',zip='${zip}',phone='${phone}',email='${email}',password='${password}' where id = ${id}`


    db.query(statement, (error, data) => {

      const stat = `update hospitals set hospital_name='${hospital_name}' where  hospital_id=${hospital_id}`
      db.query(stat, (error, data) => {



        response.send(utils.createResult(error, data))
      })

    })
  })
  // ----------------------------------------------------



  // ----------------------------------------------------
  // DELETE
  // ----------------------------------------------------

  router.delete('/:id', (request, response) => {
    const { id } = request.params
    const statement = `delete from user where id = ${id}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })




  router.delete('/deleteDoctorAppointment/:id', (request, response) => {
    const { id } = request.params
    const statement = ` delete from appointment where id = ${id}`
    db.query(statement, (error, data) => {
      if (error) {
        response.send({ status: 'error', error: error })
      } else {
        response.send(utils.createResult(error, data))

      }
    })
  })

  // ----------------------------------------------------

  module.exports = router