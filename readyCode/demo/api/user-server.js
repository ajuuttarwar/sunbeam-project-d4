const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const jwt = require('jsonwebtoken')
const config = require('./config')
const cors = require('cors')

const mailer = require('./mailer')
const uuid = require('uuid')
const fs = require('fs')
const path = require('path')

// routers
const userRouter = require('./user/routes/user')



const app = express()
app.use(cors('*'))

app.use(bodyParser.json())
app.use(morgan('combined'))





function getUserId(request, response, next) {

    if (request.url == '/user/signin' || request.url == '/user/signup' || request.url == '/user/profile-by-email'   || request.url == '/user/reset-after-forgot' || request.url.startsWith('/user/activate')
    || request.url.startsWith('/user/image/')|| request.url.startsWith('/user/forgot-password')) {

        next()
    } else {
  
      try {
        const token = request.headers['token']
        const data = jwt.verify(token, config.secret)
  
        request.userId = data['id']
  
        next()
        
      } catch (ex) {
        response.status(401)
        response.send({status: 'error', error: 'protected api'})
      }
    }
  }



app.use(getUserId)

app.use(express.static('images/'))
// add the routes
app.use('/user', userRouter)





// default route
app.get('/', (request, response) => {
    response.send('welcome to my user application')
  })
  
  app.listen(5000, '0.0.0.0', () => {
    console.log('server started on port 5000')
  })