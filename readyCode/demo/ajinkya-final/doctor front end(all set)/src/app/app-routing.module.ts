import { AppointmentsComponent } from './appointments/appointments.component';
import { AuthService } from './auth/auth.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';

import { OffersListComponent } from './offers-list/offers-list.component';
import { ArticleListComponent } from './article-list/article-list.component';
import { HospitalListComponent } from './hospital-list/hospital-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DoctorProfileComponent } from './doctor-profile/doctor-profile.component'
import { ArticleAddComponent } from './article-add/article-add.component';
import { ChangePasswordComponent } from './doctor-profile/change-password/change-password.component';
import { DoctorEditComponent } from './doctor-profile/doctor-edit/doctor-edit.component';
import { UploadCertificateComponent } from './doctor-profile/upload-certificate/upload-certificate.component';
import { OfferAddComponent } from './offers-list/offer-add/offer-add.component';
import { UpLoadArticleComponent } from './article-list/up-load-article/up-load-article.component';
import { HospitalAddComponent } from './hospital-list/hospital-add/hospital-add.component';
import { UploadProfileComponent } from './doctor-profile/upload-profile/upload-profile.component';
import { FooterComponent } from './footer/footer.component';
import { AboutUsComponent } from './about-us/about-us.component';



const routes: Routes = [
  {path : '',redirectTo:'/auth/login', pathMatch: 'full'},
  {path : 'home',
   component : HomeComponent,
    canActivate : [AuthService],
    children: [
        {path:'dashboard',component:DashboardComponent},
        {path: 'doctor-profile', component : DoctorProfileComponent},
        {path: 'hospital', component : HospitalListComponent},
        {path: 'article-list', component : ArticleListComponent},
        {path: 'offer-list', component : OffersListComponent},
        {path: 'offer-list/offer-add', component : OffersListComponent},
        {path: 'article-add', component : ArticleAddComponent},
        {path: 'doctor-profile/changePassword', component : ChangePasswordComponent},
        {path: 'doctor-profile/doctor-edit', component : DoctorEditComponent},
        {path: 'doctor-profile/uploadCertificate', component : UploadCertificateComponent},
        {path: 'doctor-profile/uploadProfile', component : UploadProfileComponent},
        {path: 'offer-list/uploadOffer', component : OfferAddComponent},
        {path: 'article-list/upLoadArticle', component : UpLoadArticleComponent},
        {path: 'hospital/addHospital', component : HospitalAddComponent},
        {path: 'appointments', component : AppointmentsComponent},
        {path: 'about-us', component : AboutUsComponent}

    ]
    
  },
  { path: 'auth', loadChildren:  () => import('./auth/auth.module').then(m => m.AuthModule)}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
