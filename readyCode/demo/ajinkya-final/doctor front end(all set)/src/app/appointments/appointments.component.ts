import { AppointmentService } from './../appointment.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.css']
})
export class AppointmentsComponent implements OnInit {

  appointments=[]
  constructor(  private router: Router,private appointmentService:AppointmentService) { }

  ngOnInit(): void {
    this.appointmentService
    .getAppointment()
    .subscribe(response => {
      if (response['status'] == 'success') {
        this.appointments = response['data']
        console.log(this.appointments)
      } else {
        console.log(response['error'])
      }
    })
  }


  toggleActive(appointment) {
    console.log(appointment)
    this.appointmentService
      .toggleActiveStatus(appointment)
      .subscribe(response => {
        if (response['status'] == 'success') {
           this.ngOnInit()
        } else {
          console.log(response['error'])
        }
      }) 
  }

}
