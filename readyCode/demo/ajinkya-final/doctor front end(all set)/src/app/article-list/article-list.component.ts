import { Component, OnInit } from '@angular/core';
import { ArticleService } from './../article.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {

  articles = []
  
  constructor(
    private router: Router,
    private articleService: ArticleService) { }

  ngOnInit(): void {
    this.loadArticles()
  }

  loadArticles() {
    this.articleService
      .getArticles()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.articles = response['data']
          console.log(this.articles)
        } else {
          console.log(response['error'])
        }
      })
  }

  onEdit(article) {
    this.router.navigate(['/home/article-edit'], {queryParams: {id: article['id']}})
  }

  addArticle() {
    this.router.navigate(['/home/article-add'])
  }
  uploadImage(article) {
    this.router.navigate(['/home/article-list/upLoadArticle'], {queryParams: {id: article['article_id']}})
  }
  onDelete(id){
    this.articleService
      .deleteArticles(id)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.articles = response['data']
          console.log('success')
         this.ngOnInit()
        } else {
          console.log(response['error'])
          alert('you are not authorized to delete this article')
        }
      })
  }
}
