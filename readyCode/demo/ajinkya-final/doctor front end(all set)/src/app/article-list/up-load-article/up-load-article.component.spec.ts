import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpLoadArticleComponent } from './up-load-article.component';

describe('UpLoadArticleComponent', () => {
  let component: UpLoadArticleComponent;
  let fixture: ComponentFixture<UpLoadArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpLoadArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpLoadArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
