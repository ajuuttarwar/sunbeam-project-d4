import { Component, OnInit } from '@angular/core';
import { ArticleService } from './../../article.service';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-up-load-article',
  templateUrl: './up-load-article.component.html',
  styleUrls: ['./up-load-article.component.css']
})
export class UpLoadArticleComponent implements OnInit {
  selectedFile = null

  constructor(
    private router: Router,
    private articleService: ArticleService,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit(): void {
  }
  onImageSelect(event) {
    this.selectedFile = event.target.files[0]
  }

  uploadImage() {
    const id = this.activatedRoute.snapshot.queryParams.id
    this.articleService
      .uploadArticle(id, this.selectedFile)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.router.navigate(['/home/article-list'])
        } else {
          console.log(response['error'])
        }
      })

  }
}
