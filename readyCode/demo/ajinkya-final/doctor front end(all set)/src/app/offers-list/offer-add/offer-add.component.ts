import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { OfferService } from './../../offer.service';

@Component({
  selector: 'app-offer-add',
  templateUrl: './offer-add.component.html',
  styleUrls: ['./offer-add.component.css']
})
export class OfferAddComponent implements OnInit {
  selectedFile = null

  constructor(
    private router: Router,
    private offerService: OfferService,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit(): void {
  }
  onImageSelect(event) {
    this.selectedFile = event.target.files[0]
  }

  onUploadImage() {
    this.offerService
      .uploadImage( this.selectedFile)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.router.navigate(['/home/offer-list'])
        } else {
          console.log(response['error'])
        }
      })
  }
}
