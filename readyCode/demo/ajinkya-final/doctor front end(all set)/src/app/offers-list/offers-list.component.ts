import { Component, OnInit } from '@angular/core';
import { OfferService } from './../offer.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-offers-list',
  templateUrl: './offers-list.component.html',
  styleUrls: ['./offers-list.component.css']
})
export class OffersListComponent implements OnInit {


  offers = []
  

  constructor(
    private router: Router,
    private offerService: OfferService) { }

  ngOnInit(): void {
    this.loadOffers()
  }

  loadOffers() {
    this.offerService
      .getOffers()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.offers = response['data']
          console.log(this.offers)
        } else {
          console.log(response['error'])
        }
      })
  }

  updateOffer(offer) {
    this.router.navigate(['/home/offer-list/offer-edit'], {queryParams: {id: offer['id']}})
  }

  addOffer() {
    this.router.navigate(['/home/offer-list/uploadOffer'])
  }

   onDelete(offer){

    const id =offer['id']

    console.log(id)
    
    this.offerService
      .deleteOffers(id)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.offers = response['data']
          console.log(this.offers)
          this.ngOnInit()
        } else {
          console.log(response['error'])
        }
      })
  }
}

