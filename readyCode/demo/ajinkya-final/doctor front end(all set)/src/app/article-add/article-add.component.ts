import { Component, OnInit } from '@angular/core';
import { ArticleService } from './../article.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-article-add',
  templateUrl: './article-add.component.html',
  styleUrls: ['./article-add.component.css']
})
export class ArticleAddComponent implements OnInit {
  selectedFile = null

  articles = []

  writer_id = 0
  article_title = ''
  article_description = ''
  constructor(
    private router: Router,
    private articleService: ArticleService) { }

  ngOnInit(): void {
    this.writer_id = sessionStorage['id']
  }
  addArticle() {
    this.writer_id = sessionStorage['id']
    this.articleService
      .insertArticle(this.writer_id, this.article_title, this.article_description)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.router.navigate(['/home/article-list'])
        }
      })
  }
}
