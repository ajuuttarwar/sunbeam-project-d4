
import { DoctorProfileService } from './doctor-profile.service';
import { HospitalService } from './hospital.service';
import { OfferService } from './offer.service';
import { ArticleService } from './article.service';
import { DoctorService } from './doctor.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HospitalListComponent } from './hospital-list/hospital-list.component';
import { ArticleListComponent } from './article-list/article-list.component';
import { OffersListComponent } from './offers-list/offers-list.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DoctorProfileComponent } from './doctor-profile/doctor-profile.component';
import { ArticleAddComponent } from './article-add/article-add.component';
import { ChangePasswordComponent } from './doctor-profile/change-password/change-password.component';
import { DoctorEditComponent } from './doctor-profile/doctor-edit/doctor-edit.component';
import { UploadCertificateComponent } from './doctor-profile/upload-certificate/upload-certificate.component';
import { OfferAddComponent } from './offers-list/offer-add/offer-add.component';
import { UpLoadArticleComponent } from './article-list/up-load-article/up-load-article.component';
import { HospitalAddComponent } from './hospital-list/hospital-add/hospital-add.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { UploadProfileComponent } from './doctor-profile/upload-profile/upload-profile.component';
import { FooterComponent } from './footer/footer.component';
import { AboutUsComponent } from './about-us/about-us.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    HospitalListComponent,
    ArticleListComponent,
    OffersListComponent,
    DoctorProfileComponent,
    ArticleAddComponent,
    ChangePasswordComponent,
    DoctorEditComponent,
    UploadCertificateComponent,
    OfferAddComponent,
    UpLoadArticleComponent,
    HospitalAddComponent,
    AppointmentsComponent,
    UploadProfileComponent,
    FooterComponent,
    AboutUsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    DoctorService,
    ArticleService,
    OfferService,
    HospitalService,
    DoctorProfileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
