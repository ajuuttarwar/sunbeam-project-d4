import { Component, OnInit } from '@angular/core';
import { DoctorProfileService } from './../../doctor-profile.service';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-doctor-edit',
  templateUrl: './doctor-edit.component.html',
  styleUrls: ['./doctor-edit.component.css']
})
export class DoctorEditComponent implements OnInit {

  doctors = []
  firstName = ''
  lastName = ''
  specialist = ''
  email = ''
  consultancy_fees= 0
  constructor(
    private router: Router,
    private doctorProfileService: DoctorProfileService,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.queryParams.id
    this.doctorProfileService
    .getDoctorsById(id)
    .subscribe(response => {
      if (response['status'] == 'success') {
        const doctor = response['data']
       
        this.doctors=doctor[0]
      
     

        this.firstName=this.doctors['firstName']
        this.lastName=this.doctors['lastName']
        this.specialist=this.doctors['specialist']
        this.email=this.doctors['email']

        this.consultancy_fees=this.doctors['price']
        console.log(this.consultancy_fees)

      } else {
        console.log(response['error'])
      }
    })
  }

  editDoctor(){
    this.doctorProfileService
    .updateDoctor(this.activatedRoute.snapshot.queryParams.id,  this.firstName, this.lastName, this.specialist, this.email, this.consultancy_fees)
    .subscribe(response => {
      if (response['status'] == 'success') {
        this.doctors = response['data']
        alert('Updated successfully')
        this.router.navigate(['/home/doctor-profile'])
      } else {
        alert('Try again')
        this.router.navigate(['/home/doctor-profile'])
      }
    })
  }

}
