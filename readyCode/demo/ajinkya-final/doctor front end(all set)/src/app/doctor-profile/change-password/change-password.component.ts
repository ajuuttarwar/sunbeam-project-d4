import { Component, OnInit } from '@angular/core';
import { DoctorProfileService } from './../../doctor-profile.service';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  
  doctors=[];
  password = ''
  confirm_password=''

  constructor(
    private router: Router,
    private doctorProfileService: DoctorProfileService,
    private activatedRoute: ActivatedRoute
    ) { }


  ngOnInit(): void {
    
  }
  changePassword()
  {
    this.doctorProfileService
    .updatePassword(this.activatedRoute.snapshot.queryParams.id,  this.password, this.confirm_password)
    .subscribe(response => {
      if (response['status'] == 'success') {
        this.doctors = response['data']
        this.router.navigate(['/auth/login'])
      } else {
        this.router.navigate(['/home/doctor-profile'])
      }
    })
}
  
}
