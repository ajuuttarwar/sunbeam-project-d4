import { DoctorProfileService } from './../../doctor-profile.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-upload-profile',
  templateUrl: './upload-profile.component.html',
  styleUrls: ['./upload-profile.component.css']
})
export class UploadProfileComponent implements OnInit {
  selectedFile = null

  constructor(
    private router: Router,
    private doctorProfileService: DoctorProfileService,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit(): void {
  }
  onImageSelect(event) {
    this.selectedFile = event.target.files[0]
  }

  onUploadProfile() {
    const id = this.activatedRoute.snapshot.queryParams['id']
    console.log(this.selectedFile)
    this.doctorProfileService
      .uploadProfile(id, this.selectedFile)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.router.navigate(['/home/doctor-profile'])
        } else {
          console.log(response['error'])
        }
      })
  }

}
