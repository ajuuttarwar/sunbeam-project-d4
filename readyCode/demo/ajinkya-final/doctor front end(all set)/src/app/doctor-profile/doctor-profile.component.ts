import { DoctorProfileService } from './../doctor-profile.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-doctor-profile',
  templateUrl: './doctor-profile.component.html',
  styleUrls: ['./doctor-profile.component.css']
})
export class DoctorProfileComponent implements OnInit {

  doctors=[];

  constructor(
    private router: Router,
    private doctorProfileService: DoctorProfileService) { }

  ngOnInit(): void {
    this.loadDoctors()
  }
  
  loadDoctors() {
    const id = sessionStorage['id']
    this.doctorProfileService
      .getDoctorsById(id)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.doctors = response['data']
          console.log(this.doctors)
        } else {
          console.log(response['error'])
        }
      })
  }

  toggleActive(doctor) {
    console.log(doctor)
    this.doctorProfileService
      .toggleActiveStatus(doctor)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.loadDoctors()
        } else {
          console.log(response['error'])
        }
      }) 
  }
  onChange(doctor) {
    console.log(doctor)
    this.router.navigate(['/home/doctor-profile/changePassword'], {queryParams: {id: doctor['id']}})
  }
    onEdit(doctor) {
    // console.log(doctor)
    this.router.navigate(['/home/doctor-profile/doctor-edit'], {queryParams: {id: doctor['id']}})
  }
  uplaodCertificate(doctor) {
    this.router.navigate(['/home/doctor-profile/uploadCertificate'], {queryParams: {id: doctor['id']}})
  }
  uplaodProfile(doctor) {
    this.router.navigate(['/home/doctor-profile/uploadProfile'], {queryParams: {id: doctor['id']}})
  }
}
