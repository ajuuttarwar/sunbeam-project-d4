import { Component, OnInit } from '@angular/core';
import { DoctorProfileService } from './../../doctor-profile.service';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-upload-certificate',
  templateUrl: './upload-certificate.component.html',
  styleUrls: ['./upload-certificate.component.css']
})
export class UploadCertificateComponent implements OnInit {
  selectedFile = null

  constructor(
    private router: Router,
    private doctorProfileService: DoctorProfileService,
    private activatedRoute: ActivatedRoute
    ) { }


  ngOnInit(): void {
  }
  onImageSelect(event) {
    this.selectedFile = event.target.files[0]
  }

  onUploadImage() {
    const id = this.activatedRoute.snapshot.queryParams['id']
    console.log(this.selectedFile)
    this.doctorProfileService
      .uploadImage(id, this.selectedFile)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.router.navigate(['/home/doctor-profile'])
        } else {
          console.log(response['error'])
        }
      })
  }
}
