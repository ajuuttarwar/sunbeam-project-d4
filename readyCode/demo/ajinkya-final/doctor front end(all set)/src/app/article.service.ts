import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  url = 'http://localhost:5001/articles'

  constructor(private httpClient: HttpClient) { }


  getArticles() {
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };
   return this.httpClient.get(this.url, httpOptions)
 }


 insertArticle(writer_id: number, article_title: string , article_description: string) {
   const httpOptions = {
    headers: new HttpHeaders({
      token: sessionStorage['token']
    })
  };
  const body = {
    writer_id: writer_id,
    article_title: article_title,  
    article_description: article_description,
  }
  return this.httpClient.post(this.url + "/add", body, httpOptions)
}

 getArticleDetails(id) {
   const httpOptions = {
    headers: new HttpHeaders({
      token: sessionStorage['token']
    })
  };
  
  return this.httpClient.get(this.url + "/details/" + id, httpOptions)
 }

 updateArticle(id, title: string, description: string) {
  const httpOptions = {
   headers: new HttpHeaders({
     token: sessionStorage['token']
   })
 };

 const body = {
   title: title,
   description: description,
 }
 
 return this.httpClient.put(this.url + "/" + id, body, httpOptions)
}
// uploadImage(file) {
//   const httpOptions = {
//     headers: new HttpHeaders({
//       token: sessionStorage['token']
//     })
//   };
//   const body = new FormData()
//   body.append('articleImage', file)

//   return this.httpClient.post(this.url + "/uploadArticle", body, httpOptions)
// }

deleteArticles(id) {
  const httpOptions = {
    headers: new HttpHeaders({
      token: sessionStorage['token']
    })
  };
  return this.httpClient.delete(this.url + "/delete/" + id  ,  httpOptions)
}


uploadArticle(id,file) {
  const httpOptions = {
    headers: new HttpHeaders({
      token: sessionStorage['token'],
    })
  };
  const body = new FormData()
  body.append('image', file)
  return this.httpClient.put(this.url + "/uploadArticle/"+ id , body, httpOptions)
}
}

