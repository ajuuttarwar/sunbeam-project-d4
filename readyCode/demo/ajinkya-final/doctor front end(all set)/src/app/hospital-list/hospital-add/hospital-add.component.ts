import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { HospitalService } from './../../hospital.service';

@Component({
  selector: 'app-hospital-add',
  templateUrl: './hospital-add.component.html',
  styleUrls: ['./hospital-add.component.css']
})
export class HospitalAddComponent implements OnInit {

  hospital_name = ''
  hospital_address = ''
  hospital_phone = ''
  hospital_email = ''

  hospital = null

  constructor(
    private router: Router,
    private hospitalService: HospitalService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.queryParams.id
    console.log(id)
    
    if (id) {
      this.hospitalService
        .getSpecificHospital(id)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const hospitals = response['data']
            // console.log(hospitals['hospital_id'])
            if (hospitals.length > 0) {
              // const id = this.activatedRoute.snapshot.queryParams.id
              this.hospital = hospitals[0]
              console.log(this.hospital)
              this.hospital_name = this.hospital['hospital_name']
              this.hospital_address = this.hospital['hospital_address']
              this.hospital_phone = this.hospital['hospital_phone']
              this.hospital_email = this.hospital['hospital_email']
            }
          }
        })
    }
  }
  onUpdateHospital(){
    if (this.hospital) {
      // edit
      console.log(this.hospital['hospital_phone'])
      this.hospitalService
        .updateHospital(this.hospital['hospital_id'], this.hospital_name, this.hospital_address, this.hospital_phone, this.hospital_email)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/home/hospital'])
          }
        })
    } else {
      const id = sessionStorage['id']
      // insert
      this.hospitalService
        .insertHospital(id, this.hospital_name,this.hospital_address,this.hospital_phone, this.hospital_email)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/home/hospital'])
          }
        })
    }

  }

}
