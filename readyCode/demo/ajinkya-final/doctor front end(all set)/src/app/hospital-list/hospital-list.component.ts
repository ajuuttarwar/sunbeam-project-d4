import { HospitalService } from './../hospital.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hospital-list',
  templateUrl: './hospital-list.component.html',
  styleUrls: ['./hospital-list.component.css']
})
export class HospitalListComponent implements OnInit {

  hospitals = []

  constructor(
    private router: Router,
    private hosptialService: HospitalService) { }

  ngOnInit(): void {
    this.loadHosptials()
  }

  loadHosptials() {
    this.hosptialService
      .getHospitals()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.hospitals = response['data']
          console.log(this.hospitals)
        } else {
          console.log(response['error'])
        }
      })
  }

  onEditHospital(hospital) {
    this.router.navigate(['/home/hospital/addHospital'], {queryParams: {id: hospital['hospital_id']}})
  }

  onAddHospital() {
    this.router.navigate(['/home/hospital/addHospital']) 
   }



   onDelete(hospital){
     const hospital_id=hospital['hospital_id']
    this.hosptialService
    .deleteHospitals(hospital_id)
    .subscribe(response => {
      if (response['status'] == 'success') {
        this.hospitals = response['data']
        alert('success')
        this.ngOnInit()
      } else {
        console.log(response['error'])
      }
    })
   }
}

