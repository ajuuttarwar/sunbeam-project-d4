import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class HospitalService {
  url = 'http://localhost:5001/hospital'

  constructor(private httpClient: HttpClient) { }
  
  getHospitals() {
    const id = sessionStorage['id']
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.get(this.url + "/details/" + id , httpOptions)
  }


  deleteHospitals(hospital_id){

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.delete(this.url + "/delete/" + hospital_id , httpOptions)
  }

  getSpecificHospital(id){
  
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.get(this.url + "/detailsHospittal/" + id , httpOptions)
  }

  // getHospitals(id) {
  //   const id = sessionStorage['id']
  //   const httpOptions = {
  //     headers: new HttpHeaders({
  //       token: sessionStorage['token']
  //     })
  //   };
  //   return this.httpClient.get(this.url + "/details/" + id , httpOptions)
  // }
  updateHospital(hospital_id: number, hospital_name: string, hospital_address: string, hospital_phone: string, hospital_email: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token'],
      }),
    };
    const body = {
      hospital_name: hospital_name,
      hospital_address: hospital_address,
      hospital_phone: hospital_phone,
      hospital_email: hospital_email
    };
    return this.httpClient.put(this.url + "/edit/" + hospital_id, body, httpOptions);

  }
  insertHospital(id :number, hospital_name : string, hospital_address: string, hospital_phone: string, hospital_email: string)
  {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token'],
      }),
    };
    const body = {
      hospital_name: hospital_name,
      hospital_address: hospital_address,
      hospital_phone: hospital_phone,
      hospital_email: hospital_email
    };
    return this.httpClient.post(this.url + "/add/" + id, body, httpOptions);
  }
}
