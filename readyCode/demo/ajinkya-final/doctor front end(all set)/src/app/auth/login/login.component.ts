import { DoctorService } from './../../doctor.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = ''
  password = ''
  id=null
  constructor(
    private router: Router,
    private doctorService: DoctorService) {}

  ngOnInit(): void {
  }

  onLogin() {
    this.doctorService
      .login(this.email, this.password)
      .subscribe(response => {
        if (response['status'] == 'success') {
          const data = response['data']
          console.log('it here')
          console.log(data)
          sessionStorage['token'] = data['token']
          sessionStorage['firstName'] = data['firstName']
          sessionStorage['lastName'] = data['lastName']
          sessionStorage['id'] = data['id']

          this.router.navigate(['/home/dashboard'])
        } else {
          alert('invalid email or password')
        }
      })
  }

}


