import { DoctorService } from './../../doctor.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(
    private router: Router,
    private doctorService: DoctorService) {}

  firstName = ''
  lastName = ''
  email = ''
  password = ''
  ngOnInit(): void {
  }
  onSignup(){
    this.doctorService
    .signup(this.firstName, this.lastName,this.email, this.password)
    .subscribe(response => {
      if (response['status'] == 'success') {
        alert('Sign Up successfully')
        this.router.navigate(['/auth/login'])
      }else{
        alert('Try once again')
        this.router.navigate(['/auth/signup'])
      }
    })
  }

  onDelete(doctor_id){
    this.doctorService
    .deleteProfile(doctor_id)
    .subscribe(response=>{
      if (response['status'] == 'success') 
        this.router.navigate(['/auth/login'])
      else
        this.router.navigate(['/home/doctor-profile'])
    })
  }
}
