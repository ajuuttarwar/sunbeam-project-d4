import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DoctorProfileService {

  url = 'http://localhost:5001/doctor'

  constructor(private httpClient: HttpClient) { }

  getDoctors() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.get(this.url + "/profile", httpOptions)
  }

  getDoctorsById(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.get(this.url + "/profile"+id, httpOptions)
  }

  updatePassword(id: number, password: string, confirm_password: string) {
    const body = {
      password: password,
      confirm_password: confirm_password
    }
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.put(this.url + "/changePassword/" + id, body, httpOptions)
  }

  updateDoctor(id: number, firstName: string, lastName: string, specialist: string, email: string, consultancy_fees ) {
    const body = {
      firstName: firstName,
      lastName: lastName,
      specialist: specialist,
      email: email,
      consultancy_fees: consultancy_fees
    }
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.put(this.url + "/" + id, body, httpOptions)
  }
  toggleActiveStatus(doctor) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      active: doctor['active'] == 1 ? 0 : 1
    }
      return this.httpClient.put(this.url + "/status/" + doctor['id'], body, httpOptions)
  }

  uploadImage(id, file) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = new FormData()
    body.append('image', file)

    return this.httpClient.put(this.url + "/uploadCertificate/" +id, body, httpOptions)
  }

  uploadProfile(id, file) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = new FormData()
    body.append('image', file)

    return this.httpClient.put(this.url + "/uploadProfile/" +id, body, httpOptions)
  }
}
