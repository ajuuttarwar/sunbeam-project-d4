import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class OfferService {

  url = 'http://localhost:5001/offers'

  constructor(private httpClient: HttpClient) { }


  getOffers() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.get(this.url, httpOptions)
  }

  updateOffer(id, title: string, description: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      title: title,
      description: description,
    }

    return this.httpClient.put(this.url + "/" + id, body, httpOptions)
  }


  deleteOffers(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
  
    return this.httpClient.delete(this.url + "/delete/" + id  ,  httpOptions)
  }

  uploadImage(file) {
    const id=sessionStorage['id']
    console.log(id)
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token'],
      })
    };
    const body = new FormData()
    body.append('image', file)
    body.append('id',id)
    return this.httpClient.post(this.url + "/uploadOffer" , body, httpOptions)
  }
}


