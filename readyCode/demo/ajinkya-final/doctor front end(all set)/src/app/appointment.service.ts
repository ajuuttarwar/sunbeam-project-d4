import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {
  url = 'http://localhost:5001/appointment'

  constructor(private httpClient: HttpClient) { }

  getAppointment() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.get(this.url, httpOptions)
  }

  toggleActiveStatus(appointment) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      active: appointment['isApproved'] == 1 ? 0 : 1
    }
      return this.httpClient.put(this.url + "/status/" + appointment['id'], body, httpOptions)
  }

}
