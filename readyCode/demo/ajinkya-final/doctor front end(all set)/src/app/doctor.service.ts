import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  url = 'http://localhost:5001/doctor'

  constructor(
    private router: Router,
    private httpClient: HttpClient) { }
    
    login(email: string, password: string) {
      const body = {
        email: email,
        password: password
      }
      return this.httpClient.post(this.url + '/signin', body)
    }
    
    signup(firstName: string, lastName: string, email: string, password: string) {
      const body = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password
      }
      return this.httpClient.post(this.url + "/signup", body)
    }
    deleteProfile(doctor_id: number){
      return this.httpClient.delete(this.url +"/" +doctor_id)

    }
}
