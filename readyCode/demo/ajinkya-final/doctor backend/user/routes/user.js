const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')

const router = express.Router()


// ----------------------------------------------------
// GET
// ----------------------------------------------------


router.get('/get-all-articles', (request, response) => {
  const statement = `select *  from articles `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})

router.get('/get-all-ngo', (request, response) => {
  const statement = `select *  from ngo `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})

router.get('/get-ngo-by-city', (request, response) => {
  
  const{ ngo_city}=request.body
  const statement = `select *  from ngo where ngo_city='${ngo_city}' `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})





router.get('/get-all-doctors', (request, response) => {
  const statement = `select firstName, lastName, email, phone from doctor `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})


router.get('/get-specific-doctor', (request, response) => {
  const { specialist} =request.body
  const statement = `select firstName, lastName, email, phone from doctor where specialist='${specialist}' `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})

router.get('/get-all-hospital', (request, response) => {
  const statement = `select firstName, lastName, email, phone from hospitals `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})

router.get('/get-doctor-by-city-speciality', (request, response) => {

  const{specialist,city}=request.body
  const statement = `select firstName, lastName, email, phone from doctor  where city='${city}' and specialist='${specialist}' `
  db.query(statement, (error, data) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      response.send(utils.createResult(error, data))

    }
  })
})





router.get('/profile', (request, response) => {
  const statement = `select firstName, lastName, email, phone from user where id = ${request.userId}`
  db.query(statement, (error, users) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (users.length == 0) {
        response.send({ status: 'error', error: 'user does not exist' })
      } else {
        const user = users[0]
        response.send(utils.createResult(error, user))
      }
    }
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------
//signup

router.post('/signup', (request, response) => {
  const { firstName, lastName, email, password } = request.body

  const encryptedPassword = crypto.SHA256(password)
  const statement = `insert into user (firstName, lastName, email, password) values (
    '${firstName}', '${lastName}', '${email}', '${encryptedPassword}'
  )`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})




//signin-post

router.post('/signin', (request, response) => {
  const { email, password } = request.body
  const statement = `select id, firstName, lastName from user where email = '${email}' and password = '${crypto.SHA256(password)}'`
  db.query(statement, (error, users) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (users.length == 0) {
        response.send({ status: 'error', error: 'user does not exist' })
      } else {
        const user = users[0]
        const token = jwt.sign({ id: user['id'] }, config.secret)
        response.send(utils.createResult(error, {
          firstName: user['firstName'],
          lastName: user['lastName'],
          token: token
        }))
      }
    }
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put('/edit-user/:id', (request, response) => {
  const { id } = request.params

  const { firstName, lastName,address,city,country,zip,phone } = request.body

  // const encryptedPassword = crypto.SHA256(password)
  const statement = `UPDATE user set firstName='${firstName}', lastName= '${lastName}',address='${address}',city='${city}',country='${country}',zip='${zip}',phone='${phone}' where id = ${id}`


  db.query(statement, (error, data) => {

    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('/:id', (request, response) => {
  const { id } = request.params
  const statement = `delete from user where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})


// ----------------------------------------------------

module.exports = router