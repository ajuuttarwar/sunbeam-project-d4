const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const crypto = require('crypto-js')

const router = express.Router()



// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/details/:id', (request, response) => {
    const { id } = request.params
    const statement = `select d.id, d.firstName, d.lastName, h.hospital_id,h.hospital_phone, h.hospital_name, h.hospital_address, h.hospital_email from doctor d inner join hospitals h on d.id = h.doctor_id having id=${id}`
       db.query(statement, (error, data)=>{
           response.send(utils.createResult(error,data))
       })
    })

    router.get('/detailsHospittal/:id', (request, response) => {
        const { id } = request.params
        // const statement = `select d.id, d.firstName, d.lastName, h.hospital_id,h.hospital_phone, h.hospital_name, h.hospital_address, h.hospital_email from doctor d inner join hospitals h on d.id = h.doctor_id having id=${id}`
        const statement=`select * from hospitals where hospital_id=${id}`
           db.query(statement, (error, data)=>{
               response.send(utils.createResult(error,data))
           })
        })

// ----------------------------------------------------
// POST
// ----------------------------------------------------

router.post('/add/:id', (request, response) => {
    const { id } = request.params
    const { hospital_name, hospital_address, hospital_phone, hospital_email } = request.body
    const statement = `insert into hospitals( doctor_id, hospital_name, hospital_address, hospital_phone, hospital_email) values (
    '${id}', '${hospital_name}','${hospital_address}', '${hospital_phone}','${hospital_email}')`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})
// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put('/edit/:hospital_id', (request, response) => {
    const { hospital_id } = request.params
    const { hospital_name, hospital_address, hospital_phone, hospital_email } = request.body
    const statement = `UPDATE hospitals set hospital_name ='${hospital_name}', hospital_address= '${hospital_address}', hospital_phone= '${hospital_phone}', hospital_email= '${hospital_email}' where hospital_id = '${hospital_id}'`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})
// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('/delete/:hospital_id', (request, response) => {
    const { hospital_id } = request.params
    const statement = `delete from hospitals where hospital_id = ${hospital_id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router