const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const { request } = require('express')
const router = express.Router()
const multer = require('multer')
const fs = require('fs')

const upload = multer({ dest: 'images/' })



router.get('/image/:filename', (request, response) => {
    const {filename} = request.params
    const file = fs.readFileSync(__dirname + '/../../images/' + filename)
    response.send(file)
  })
  
  router.put('/uploadArticle/:id', upload.single('image'), (request, response) => {
    const {id} = request.params
    const fileName = request.file.filename
    const statement = `update articles set article_image ='${fileName}' where article_id= '${id}'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

router.post('/add', (request, response)=>
{
    const{ writer_id, article_title, article_description}= request.body
    const statement = `insert into articles (writer_id, article_title, article_description) values('${writer_id}', '${article_title}', '${article_description}')`
    db.query(statement, (error,data)=>{
        response.send(utils.createResult(error,data))
    })
})


//GET- Get all the articles
router.get('/', (request, response)=>
{
    const statement = `select * from articles`
    db.query(statement, (error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

//PUT-update existing article
router.put('/edit/:id', (request, response)=>
{
    const {id} = request.params
    const{ article_title, article__description}= request.body
    const statement = `UPDATE articles set article_title=${article_title}, article_description= ${article__description} where article_id = ${id}`
    db.query(statement, (error,data)=>{
        response.send(utils.createResult(error,data))
    })
})


//Delete existing article
router.delete('/delete/:id', (request, response) => {
    const{w_id}=request.userId
    const {id} = request.params
    const statement = `delete from articles where article_id = ${id} AND writer_id=${w_id}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

module.exports = router