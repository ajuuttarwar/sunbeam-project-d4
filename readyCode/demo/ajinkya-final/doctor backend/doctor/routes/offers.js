const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const { request } = require('express')

const router = express.Router()
const multer = require('multer')
const fs = require('fs')
const upload = multer({ dest: 'images/' })




router.get('/image/:filename', (request, response) => {
  const {filename} = request.params
  console.log('hi')
  const file = fs.readFileSync(__dirname + '/../../images/' + filename)
  response.send(file)
})





//POST-add new offers into the database table
router.post('/uploadOffer', upload.single('image'), (request, response) => {
  const { id } = request.body
  
  console.log(id)
  const fileName = request.file.filename
  const statement = `insert into offers(offer_image,doctor_id) values( '${fileName}',${id})`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

//GET- Get all the offers
router.get('/', (request, response)=>
{
    const statement = `select o.id, o.offer_image,d.firstName,d.lastName from offers o inner join doctor d on d.id=o.doctor_id ;
    `
    db.query(statement, (error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

//PUT-update existing offers
router.put('/edit:id', (request, response)=>
{
    const {id} = request.params
    const{ offer_image}= request.body
    const statement = `UPDATE offers set offer_image = ${offer_image} where id = ${id}`
    db.query(statement, (error,data)=>{
        response.send(utils.createResult(error,data))
    })
})


//Delete existing offer
router.delete('/delete/:id', (request, response) => {
  const{w_id}=request.userId
    const {id} = request.params
    const statement = `delete from offers where id = ${id} `
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

module.exports = router