const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const { request, response } = require('express')
const fs = require('fs')
const router = express.Router()
const multer = require('multer')
const upload = multer({ dest: 'images/' })



// ----------------------------------------------------
// GET
// ----------------------------------------------------
router.get('/image/:filename', (request, response) => {
  const {filename} = request.params
  console.log(__dirname)
  console.log('hi')
  const file = fs.readFileSync(__dirname + '/../../images/' + filename)
  response.send(file)
})

router.get('/profile', (request, response) => {
  const statement = ` select id,firstName, lastName, specialist,  email, phone, price,certificate, active from doctor where id=5;  `
   db.query(statement, (error, data)=>{
    response.send(utils.createResult(error,data))
  })
})


router.get('/profile:id', (request, response) => {
  const{id}=request.params
  const statement = ` select id,firstName,image, lastName, specialist,  email, phone, price,certificate, active from doctor where id=${id};  `
   db.query(statement, (error, data)=>{
    response.send(utils.createResult(error,data))
  })
})

// ----------------------------------------------------
// POST
// ----------------------------------------------------

//signup
router.post('/signup', (request, response) => {
  const { firstName, lastName,  email, password } = request.body
  const statement = `insert into doctor (firstName, lastName, email, password ) values (
    '${firstName}', '${lastName}', '${email}', '${crypto.SHA256(password)}'
  )`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

//signin
router.post('/signin', (request, response) => {
  const { email, password } = request.body
  const statement = `select id, firstName, lastName from doctor where email = '${email}' and password = '${crypto.SHA256(password)}'`
  db.query(statement, (error, doctors) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (doctors.length == 0) {
        response.send({ status: 'error', error: 'Doctor does not exist' })
      } else {
        const doctor = doctors[0]
        const token = jwt.sign({ id: doctor['id'] }, config.secret)
        response.send(utils.createResult(error, {
          // Id: doctor['id'],
          firstName: doctor['firstName'],
          lastName: doctor['lastName'],
          id:doctor['id'],
          token: token
        }))
      }
    }
  })
})


//doctors certificate 
router.put('/uploadCertificate/:id', upload.single('image'), (request, response) => {
  const { id } = request.params
  const fileName = request.file.filename
  const statement = `update doctor set certificate = '${fileName}' where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// //doctor-profile
router.put('/uploadProfile/:id', upload.single('image'), (request, response) => {
  const { id } = request.params
  const fileName = request.file.filename
  const statement = `update doctor set image = '${fileName}' where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})
// ----------------------------------------------------
// PUT
// ----------------------------------------------------
router.put('/:id', (request, response) => {
  const { id } = request.params
  const { firstName, lastName, specialist, email, consultancy_fees } = request.body
  const statement = `UPDATE doctor set firstName ='${firstName}', lastName= '${lastName}', specialist ='${specialist}', email= '${email}', price = ${consultancy_fees}  where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

//forgot password
router.put('/changePassword/:id', (request, response) => {
  const { id } = request.params
  const { password, confirm_password } = request.body
  if (password == confirm_password) {
    const statement = `update doctor set password='${crypto.SHA256(password)}' where id = '${id}'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  } else {
    response.send({ status: 'error', error: 'password not match please enter correct one' })
  }
})


//change active status
router.put('/status/:id', (request, response) => {
  const { id } = request.params
  const { active } = request.body
  const statement = `UPDATE doctor set active ='${active}' where id = '${id}'`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('/:id', (request, response) => {
  const { id } = request.params
  const statement = `delete from doctor where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})


module.exports = router