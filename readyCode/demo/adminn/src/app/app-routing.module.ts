import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthService } from './auth/auth.service';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';

const routes: Routes = [
  { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
  
  {
    path: 'home',
    component: HomeComponent,
     canActivate: [AuthService],
    children: [
      { path: 'doctor', loadChildren: () => import('./doctor/doctor.module').then(m => m.DoctorModule ) },
    { path: 'patient', loadChildren: () => import('./patient/patient.module').then(m => m.PatientModule ) },
    { path: 'ngo', loadChildren: () => import('./ngo/ngo.module').then(m => m.NgoModule ) },
    { path: 'article', loadChildren: () => import('./article/article.module').then(m => m.ArticleModule ) },
    {path:'dashboard',component: DashboardComponent},
    {path:'admin-profile',component: AdminProfileComponent },
    ],
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
