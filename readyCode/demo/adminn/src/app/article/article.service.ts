import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  url = 'http://localhost:4000/articles';

  constructor(private httpClient: HttpClient) {}

  getArticle() {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };
   
   return this.httpClient.get(this.url , httpOptions)
 }

 getArticleDetails(id) {
  // add the token in the request header
  const httpOptions = {
   headers: new HttpHeaders({
     token: sessionStorage['token']
   })
 };
 
 return this.httpClient.get(this.url + "/details/" + id, httpOptions)
}



updateArticle(id,  title: string, description: string) {
  // add the token in the request header
  const httpOptions = {
   headers: new HttpHeaders({
     token: sessionStorage['token']
   })
 };

 const body = {
  article_title: title,
  article_description: description
 }
 
 return this.httpClient.put(this.url + "/" + id, body, httpOptions)
}


insertArticle(
  title: string,
  description: string
) {
  // add the token in the request header
  const httpOptions = {
    headers: new HttpHeaders({
      token: sessionStorage['token'],
    }),
  };

  const body = {
    title: title,
    description: description,
  };

  return this.httpClient.post(this.url + '/create', body, httpOptions);
}




deleteArticle(id){
  const httpOptions = {
    headers: new HttpHeaders({
      token: sessionStorage['token']
    })
  };
  
  return this.httpClient.delete(this.url + "/" + id, httpOptions)

}

}
