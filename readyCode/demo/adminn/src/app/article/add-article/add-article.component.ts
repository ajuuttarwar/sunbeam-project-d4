import { ArticleService } from './../article.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {

 
  title=''
  description=''

  article=null

 constructor(
    private router:Router,
    private articleService:ArticleService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {

    const id = this.activatedRoute.snapshot.queryParams['id']
    if (id) {
      // edit product
      this.articleService
        .getArticleDetails(id)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const articles = response['data']
            if (articles.length > 0) {
              
              this.article=articles[0]
              console.log(this.article)
              this.title=this.article['article_title']
              this.description=this.article['article_description']
            }
          }
        })
    }
  }

  onUpdate() {

    if (this.article) {
   
// edit
     this.articleService
     .updateArticle(this.article['article_id'], this.title, this.description)
     .subscribe(response => {
       if (response['status'] == 'success') {
         this.router.navigate(['/home/article/list-article'])
       }
     })
  }else {
    // insert
    this.articleService
      .insertArticle(this.title, this.description)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.router.navigate(['/home/article/list-article'])
        }
      })
  }

}




 



}
