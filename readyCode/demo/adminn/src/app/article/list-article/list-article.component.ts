import { ArticleService } from './../article.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-article',
  templateUrl: './list-article.component.html',
  styleUrls: ['./list-article.component.css']
})
export class ListArticleComponent implements OnInit {
  articles =  []

  // firstName: "abc",
  // lastName: "abc",
  // specialist: "Dentist",
  // hospital_id: null,
  // country: null,
  // phone: null,
  // email: "abc@test.com",
  // createdOn: "2020-09-16T14:15:02.000Z",
  // active: 1

constructor(
  private router: Router,
  private articleService : ArticleService
 ) { }

 ngOnInit(): void {
  this.loadArticle()
}

loadArticle() {
  this.articleService
    .getArticle()
    .subscribe(response => {
      if (response['status'] == 'success') {
        this.articles = response['data']
        console.log(this.articles)
      } else {
        console.log(response['error'])
      }
    })
}

onEdit(article) {
  this.router.navigate(['/home/article/add-article'], {queryParams: {id: article['article_id']}})
}

onDelete(article) {

  const id=article['article_id']

  this.articleService
    .deleteArticle(id)
    .subscribe(response => {
      if (response['status'] == 'success') {
        this.ngOnInit()
        
      } else {
        console.log(response['error'])
      }
    })

}


addArticle() {
  this.router.navigate(['/home/article/add-article'])
}
}
