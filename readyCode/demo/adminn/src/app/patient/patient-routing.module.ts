import { ListPatientComponent } from './list-patient/list-patient.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPatientComponent } from './add-patient/add-patient.component';

const routes: Routes = [
  {
    path: 'add-patient',
    component: AddPatientComponent,
  },
  {
    path: 'list-patient',
    component: ListPatientComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientRoutingModule { }
