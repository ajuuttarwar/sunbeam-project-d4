import { PatientService } from './../patient.service';
import { PatientModule } from './../patient.module';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-patient',
  templateUrl: './list-patient.component.html',
  styleUrls: ['./list-patient.component.css']
})
export class ListPatientComponent implements OnInit {

  
  patients =  []

    // firstName: "abc",
    // lastName: "abc",
    // specialist: "Dentist",
    // hospital_id: null,
    // country: null,
    // phone: null,
    // email: "abc@test.com",
    // createdOn: "2020-09-16T14:15:02.000Z",
    // active: 1

  constructor(
    private router: Router,
    private patientService : PatientService
   ) { }

   ngOnInit(): void {
    this.loadPatient()
  }

  loadPatient() {
    this.patientService
      .getPatient()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.patients = response['data']
          console.log(this.patients)
        } else {
          console.log(response['error'])
        }
      })
  }

  onEdit(patient) {
    this.router.navigate(['/home/patient/add-patient'], {queryParams: {id: patient['id']}})
  }

  addDoctor() {
    this.router.navigate(['/home/patient/add-patient'])
  }

  toggleActive(patient) {
    console.log(patient)
    this.patientService
      .toggleActiveStatus(patient)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.loadPatient()
        } else {
          console.log(response['error'])
        }
      }) 
  }
}
