import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  url = 'http://localhost:4000/patient';

  constructor(private httpClient: HttpClient) {}

  getPatient() {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };
   
   return this.httpClient.get(this.url + "/allPatient" , httpOptions)
 }

 toggleActiveStatus(patient) {
  // add the token in the request header
  const httpOptions = {
    headers: new HttpHeaders({
      token: sessionStorage['token']
    })
  };
  
  // suspend user if already active or activate otherwise
  const body = {
    status: patient['active'] == 1 ? 0 : 1
  }

  return this.httpClient.put(this.url + "/toggle-patient/" + patient['id'], body, httpOptions)
}

}
