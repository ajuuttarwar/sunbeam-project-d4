// import { HomeService } from './../home.service';
// import { AuthService } from './../auth/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {


 

  

  name=sessionStorage.getItem('firstName')
  last_name=sessionStorage.getItem('lastName')

  userName= this.name + ' ' + this.last_name

  constructor(
    private router: Router
  ) {}

  ngOnInit(): void {
   
  }

  onLogout() {
    sessionStorage.removeItem('token')
    sessionStorage.removeItem('firstName')
    sessionStorage.removeItem('lastName')

    this.router.navigate(['/auth/login'])
  }

}
