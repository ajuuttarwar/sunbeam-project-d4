import { NgoService } from './../ngo/ngo.service';
import { PatientService } from './../patient/patient.service';
import { DoctorService } from './../doctor/doctor.service';
import { HomeComponent } from './../home/home.component';
import { DashboardService } from './../dashboard.service';
import { HomeService } from './../home.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  doctor = [];
  patient = [];
  appointment = [];
  ngo = [];

  constructor(private router: Router,private dashboardService:DashboardService, private doctorService:DoctorService,
    private patientService:PatientService,private ngoService:NgoService) {}
  ngOnInit(): void {
    this.onLoadDoctor();
    this.onLoadPatient();
    this.onLoadNgo();
  }

  onLoadDoctor() {
    this.doctorService.getDoctor().subscribe((response) => {
      if (response['status'] == 'success') {
        this.doctor = response['data'];
        console.log(this.doctor);
      } else {
        console.log(response['error']);
      }
    });
  }

  onLoadPatient() {
    this.patientService.getPatient().subscribe((response) => {
      if (response['status'] == 'success') {
        this.patient = response['data'];
        console.log(this.patient);
      } else {
        console.log(response['error']);
      }
    });
  }
  onLoadAppointment() {}

  // getNgo
  onLoadNgo() {

    this.ngoService.getNgo().subscribe((response) => {
      if (response['status'] == 'success') {
        this.ngo = response['data'];
        console.log(this.ngo);
      } else {
        console.log(response['error']);
      }
    });
  }
}
