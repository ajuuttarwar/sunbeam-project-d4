import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DoctorService {
  url = 'http://localhost:4000/admin';

  constructor(private httpClient: HttpClient) {}

  getDoctor() {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };
   
   return this.httpClient.get(this.url + '/allDoctor', httpOptions)
 }


 toggleActiveStatus(doctor) {
  // add the token in the request header
  const httpOptions = {
    headers: new HttpHeaders({
      token: sessionStorage['token']
    })
  };
  
  // suspend user if already active or activate otherwise
  const body = {
    status: doctor['active'] == 1 ? 0 : 1
  }

  return this.httpClient.put(this.url + "/toggle-doctor/" + doctor['id'], body, httpOptions)
}



}
