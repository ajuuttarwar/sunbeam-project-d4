import { DoctorService } from './../doctor.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-doctor',
  templateUrl: './list-doctor.component.html',
  styleUrls: ['./list-doctor.component.css']
})
export class ListDoctorComponent implements OnInit {

  doctors = []

    // firstName: "abc",
    // lastName: "abc",
    // specialist: "Dentist",
    // hospital_id: null,
    // country: null,
    // phone: null,
    // email: "abc@test.com",
    // createdOn: "2020-09-16T14:15:02.000Z",
    // active: 1

  constructor(
    private router: Router,
    private doctorService : DoctorService
   ) { }

   ngOnInit(): void {
    this.loadDoctor()
  }

  loadDoctor() {   
    this.doctorService
      .getDoctor()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.doctors = response['data']
          console.log(this.doctors)
        } else {
          console.log(response['error'])
        }
      })
  }

  onEdit(doctor) {
    this.router.navigate(['/home/doctor/add-doctor'], {queryParams: {id: doctor['id']}})
  }

  addDoctor() {
    this.router.navigate(['/home/doctor/add-doctor'])
  }


  toggleActive(doctor) {
    console.log(doctor)
    this.doctorService
      .toggleActiveStatus(doctor)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.loadDoctor()
        } else {
          console.log(response['error'])
        }
      }) 
  }
}
