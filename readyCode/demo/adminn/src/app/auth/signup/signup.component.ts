import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  firstName=''
  lastName=''
  phone=''
  email = ''
  password = ''

  constructor(
    private toastr : ToastrService,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit(): void {
  }

  // onSignup() {
  //   this.authService
  //     .signup(this.firstName,this.lastName,this.phone,this.email, this.password)
  //     .subscribe(response => {
  //       if (response['status'] == 'success') {
  //         const data = response['data']
  //         console.log(data)

  //         // // cache the user info
  //         // sessionStorage['token'] = data['token']
  //         // sessionStorage['firstName'] = data['firstName']
  //         // sessionStorage['lastName'] = data['lastName']

  //         // goto the dashboard
  //         this.router.navigate(['/home/auth/login'])

  //       } else {
  //         alert(response['error'])
  //       }
  //     })
  // }


  onSignup() {
    if(this.firstName.length == 0){
      this.toastr.error('please enter first Name')

    }else if(this.lastName.length == 0){
      this.toastr.error('please enter last Name')

    }else if(this.phone.length == 0){
      this.toastr.error('please enter phone Number')

    }
    else if(this.email.length == 0){
      this.toastr.error('please enter email')
    }else if(this.password.length == 0){
      this.toastr.error('please enter password')
    }else{
      this.authService.signup(this.firstName, this.lastName,this.phone, this.email, this.password)
    .subscribe(response => {
      if (response['status'] == 'success') {
        this.toastr.success(`You are registered successfully please login with your credential`)
        this.router.navigate(['/auth/login'])
      } else {
        this.toastr.error('error while registering')
     
      }
    })
    }
    
  }

}
