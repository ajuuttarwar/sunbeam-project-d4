import { NgoService } from './../ngo.service';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-ngo',
  templateUrl: './add-ngo.component.html',
  styleUrls: ['./add-ngo.component.css']
})
export class AddNgoComponent implements OnInit {

  
  ngo_name=''
  ngo_city=''
  ngo_country=''
  ngo_zip=''
  ngo_phone=''
  ngo_email=''
  ngo_address=''
  ngo_description=''

  ngo=null

  constructor(
    private router:Router,
    private ngoService:NgoService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void 
  {
 
    const id = this.activatedRoute.snapshot.queryParams.id
   
    
    if (id) {
      // edit product
      this.ngoService
        .getNgoDetails(id)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const ngos = response['data']
            if (ngos.length > 0) {
              this.ngo = ngos[0]
              
            // this.id= this.ngo['ngo_id']
              this.ngo_name = this.ngo['ngo_name']
              this.ngo_city = this.ngo['ngo_city']

              this.ngo_zip = this.ngo['ngo_zip']
              this.ngo_phone = this.ngo['ngo_phone']
              this.ngo_email = this.ngo['ngo_email']
              this.ngo_country = this.ngo['ngo_country']
              this.ngo_address = this.ngo['ngo_address']
              this.ngo_description = this.ngo['ngo_description']
              
            }
          }
        })
  }
  }

  onUpdate() {
    if (this.ngo) {
      // edit
      this.ngoService
        .updateProduct(this.ngo['ngo_id'], this.ngo_name,this.ngo_city,this.ngo_country,this.ngo_zip,this.ngo_phone, this.ngo_email, this.ngo_address, this.ngo_description)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/home/ngo/list-ngo'])
          }
        })
    } else {
      // insert



      this.ngoService
        .insertNgo(this.ngo_name ,this.ngo_address,this.ngo_city,this.ngo_country,this.ngo_zip,this.ngo_phone, this.ngo_email, this.ngo_description)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/home/ngo/list-ngo'])
          }
        })
    }

  }

}
