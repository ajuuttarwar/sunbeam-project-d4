import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgoRoutingModule } from './ngo-routing.module';
import { AddNgoComponent } from './add-ngo/add-ngo.component';
import { ListNgoComponent } from './list-ngo/list-ngo.component';


@NgModule({
  declarations: [AddNgoComponent, ListNgoComponent],
  imports: [
    CommonModule,
    NgoRoutingModule,
    FormsModule
  ]
})
export class NgoModule { }
