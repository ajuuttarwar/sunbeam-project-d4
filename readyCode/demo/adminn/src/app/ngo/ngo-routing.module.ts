import { ListNgoComponent } from './list-ngo/list-ngo.component';
import { AddNgoComponent } from './add-ngo/add-ngo.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'add-ngo',
    component: AddNgoComponent,
  },
  {
    path: 'list-ngo',
    component: ListNgoComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NgoRoutingModule { }
