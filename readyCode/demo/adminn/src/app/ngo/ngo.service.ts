import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class NgoService {
  url = 'http://localhost:4000/ngo';

  constructor(private httpClient: HttpClient) {}

  getNgo() {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token'],
      }),
    };

    return this.httpClient.get(this.url, httpOptions);
  }

  updateProduct(
    ngo_id :number,
    ngo_name: string,
    ngo_city: string,
    ngo_country: string,
    ngo_zip: string,
    ngo_phone: string,
    ngo_email: string,
    ngo_address: string,
    ngo_description: string
  ) {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token'],
      }),
    };

    const body = {
      ngo_name: ngo_name,
      ngo_city: ngo_city,
      ngo_country: ngo_country,
      ngo_zip: ngo_zip,
      ngo_phone: ngo_phone,
      ngo_email: ngo_email,
      ngo_address: ngo_address,
      ngo_description: ngo_description,
    };

    return this.httpClient.put(this.url + "/" + ngo_id, body, httpOptions);
  }

  insertNgo(
    ngo_name: string,
    ngo_address: string,
    ngo_city: string,
    ngo_country: string,
    ngo_zip: string,
    ngo_phone: string,
    ngo_email: string,
    
    ngo_description: string
  ) {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token'],
      }),
    };

    const body = {
      ngo_name: ngo_name,
      ngo_city: ngo_city,
      ngo_country: ngo_country,
      ngo_zip: ngo_zip,
      ngo_phone: ngo_phone,
      ngo_email: ngo_email,
      ngo_address: ngo_address,
      ngo_description: ngo_description,
    };

    return this.httpClient.post(this.url + '/create', body, httpOptions);
  }

  getNgoDetails(id) {
  
  
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token'],
      }),
    };

    return this.httpClient.get(this.url + '/' + id, httpOptions);
  }



  deleteNgo(id){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    
    return this.httpClient.delete(this.url + "/" + id, httpOptions)
  
  }
}
