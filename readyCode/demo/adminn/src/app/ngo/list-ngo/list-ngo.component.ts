import { NgoService } from './../ngo.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-ngo',
  templateUrl: './list-ngo.component.html',
  styleUrls: ['./list-ngo.component.css']
})
export class ListNgoComponent implements OnInit {

  ngos =  []
  constructor(
    private router: Router,
    private ngoService : NgoService
   ) { }

   ngOnInit(): void {
    this.loadNgo()
  }

  loadNgo() {
    this.ngoService
      .getNgo()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.ngos = response['data']
          console.log(this.ngos)
        } else {
          console.log(response['error'])
        }
      })
  }

  onEdit(ngo) {
    console.log(ngo)
    this.router.navigate(['/home/ngo/add-ngo'], {queryParams: {id: ngo['ngo_id']}})
  }


  onDelete(ngo) {

    const id=ngo['ngo_id']
  
    this.ngoService
      .deleteNgo(id)
      .subscribe(response => {
        if (response['status'] == 'success') {
         this.ngOnInit() 
          
        } else {
          console.log(response['error'])
        }
      })
  
  }

  addNgo() {
    this.router.navigate(['/home/ngo/add-ngo'])
  }

}
