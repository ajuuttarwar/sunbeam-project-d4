import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListNgoComponent } from './list-ngo.component';

describe('ListNgoComponent', () => {
  let component: ListNgoComponent;
  let fixture: ComponentFixture<ListNgoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListNgoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListNgoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
