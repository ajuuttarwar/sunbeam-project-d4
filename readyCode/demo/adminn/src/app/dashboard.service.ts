import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class DashboardService {
 

  constructor(private httpClient: HttpClient) {}

  getDoctor() {
    // add the token in the request header
    const url = 'http://localhost:4000/admin';
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };
   
   return this.httpClient.get(url + '/allDoctor', httpOptions)
 }
}
