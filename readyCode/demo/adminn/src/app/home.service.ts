import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  url = 'http://localhost:4000/admin'

  constructor(
    private router: Router,
    private httpClient: HttpClient) { }
  
    
    loadDoctor() {
           return this.httpClient.get(this.url + '/countDoctor', )
    }

}
