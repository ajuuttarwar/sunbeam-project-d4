import { NutritionComponent } from './nutrition/nutrition.component';
import { YogaComponent } from './yoga/yoga.component';
import { MeditationComponent } from './meditation/meditation.component';
import { ExcerciseComponent } from './excercise/excercise.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthService } from './auth/auth.service';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';

const routes: Routes = [
  {path : '',redirectTo:'/home', pathMatch: 'full'},
  {path : 'home',
   component : HomeComponent,
   canActivate : [AuthService],
    children: [
      { path: 'doctor', loadChildren: () => import('./doctor/doctor.module').then(m => m.DoctorModule ) },
      { path: 'hospital', loadChildren: () => import('./hospital/hospital.module').then(m => m.HospitalModule )},
      { path: 'ngo', loadChildren: () => import('./ngo/ngo.module').then(m => m.NgoModule ) },
      { path: 'article', loadChildren: () => import('./article/article.module').then(m => m.ArticleModule ) },
      { path: 'user', loadChildren: () => import('./user/user.module').then(m => m.UserModule ) },
      {path:'dashboard',component:DashboardComponent},
      {path:'about-us',component:AboutUsComponent},
      {path:'excercise',component:ExcerciseComponent},
      {path:'meditation',component:MeditationComponent},
      {path:'yoga',component:YogaComponent},
      {path:'nutrition',component:NutritionComponent}
    ]
    
  },
  { path: 'auth', loadChildren:  () => import('./auth/auth.module').then(m => m.AuthModule)}

];
@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
