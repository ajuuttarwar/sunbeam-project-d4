import { Router, ActivatedRoute } from '@angular/router';
import { HospitalService } from './../hospital.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hospital-details',
  templateUrl: './hospital-details.component.html',
  styleUrls: ['./hospital-details.component.css']
})
export class HospitalDetailsComponent implements OnInit {

  hospital=null

  constructor(
    private router: Router,private hospitalService : HospitalService,
    private activatedRoute: ActivatedRoute
  ) { }
  ngOnInit(): void {

    
    const id = this.activatedRoute.snapshot.queryParams['id']
    if (id) {
      // edit product
      this.hospitalService
        .getHospitalDetails(id)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const doctors = response['data']
            if (doctors.length > 0) {
              
              this.hospital=doctors[0]
              console.log(this.hospital)
              // this.title=this.article['article_title']
              // this.description=this.article['article_description']
            }
          }
        })
    }
  }

  onUpdate(doctor) {
    this.router.navigate(['/home/doctor/doctor-update'], {queryParams: {id: doctor['id']}})
  }

  onBack(){
    this.router.navigate(['/home/hospital/hospital-list'])
  }

}
