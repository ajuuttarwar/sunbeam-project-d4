import { HospitalDetailsComponent } from './hospital-details/hospital-details.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HospitalListComponent } from './hospital-list/hospital-list.component';

const routes: Routes = [

  
  {
    path: 'hospital-list',
    component: HospitalListComponent,
  },
  {
    path: 'hospital-details',
    component: HospitalDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HospitalRoutingModule { }
