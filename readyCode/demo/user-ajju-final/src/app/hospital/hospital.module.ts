import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HospitalRoutingModule } from './hospital-routing.module';
import { HospitalListComponent } from './hospital-list/hospital-list.component';
import { HospitalDetailsComponent } from './hospital-details/hospital-details.component';


@NgModule({
  declarations: [HospitalListComponent, HospitalDetailsComponent],
  imports: [
    CommonModule,
    HospitalRoutingModule
  ]
})
export class HospitalModule { }
