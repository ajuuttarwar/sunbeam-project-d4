import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HospitalService {

  
  url = 'http://localhost:5000/user'

  constructor(private httpClient: HttpClient) { }

  getHospitals() {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    
    return this.httpClient.get(this.url + "/get-all-hospital", httpOptions)
  }


  getHospitalDetails(id) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };
   
   return this.httpClient.get(this.url + "/get-hospital-by-id/" + id, httpOptions)
  }
 
}
