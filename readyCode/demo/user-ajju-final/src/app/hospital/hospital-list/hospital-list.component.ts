import { HospitalService } from './../hospital.service';

import { Router } from '@angular/router';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hospital-list',
  templateUrl: './hospital-list.component.html',
  styleUrls: ['./hospital-list.component.css']
})

export class HospitalListComponent implements OnInit {
  hospitals = []

  constructor( private router: Router,private hospitalService : HospitalService) { }

  ngOnInit(): void {
    this.loadDoctors()
  }
  loadDoctors() {
    this.hospitalService
      .getHospitals()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.hospitals = response['data']
        } else {
          console.log(response['error'])
        }
      })
  }

  onEdit(hospital) {
    this.router.navigate(['/home/hospital/hospital-details'], {queryParams: {id: hospital['hospital_id']}})
  }

}
