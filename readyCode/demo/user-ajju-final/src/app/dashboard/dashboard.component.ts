import { ArticleService } from './../article/article.service';
import { DoctorService } from './../doctor/doctor.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  firstName=sessionStorage.getItem('firstName')
  

  articles = []

  tempArticles=[]

  constructor(  private router: Router,
    private articleService: ArticleService) { }

  ngOnInit(): void {
    this.loadArticles()
  }

  loadArticles() {
    this.articleService
      .getArticles()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.articles = response['data']

          for (let index = 0; index < this.articles.length; index++) {

            if(index<2){
              this.tempArticles.push(this.articles[index])
            }
            
          }          
        } else {
          console.log(response['error'])
        }
      })
  }

  onChange(id) {
    if(id==1){
      this.router.navigate(['/home/doctor/search-doctor'], {queryParams: {specialist: 'dentist'}})
    }else if(id==2){
      this.router.navigate(['/home/doctor/search-doctor'], {queryParams: {specialist: 'gynecologist'}})
    }else if(id==3){
      this.router.navigate(['/home/doctor/search-doctor'], {queryParams: {specialist: 'dietitian'}})
    }else if(id==4){
      this.router.navigate(['/home/doctor/search-doctor'], {queryParams: {specialist: 'physiotherapist'}})
    }else if(id==5){
      this.router.navigate(['/home/doctor/search-doctor'], {queryParams: {specialist: 'surgeon'}})
    }else if(id==6){
      this.router.navigate(['/home/doctor/search-doctor'], {queryParams: {specialist: 'orthopedist'}})
    }
    
  }


  onSearchArticles(){
    this.router.navigate(['/home/article/article-list'])
    
  }

}
