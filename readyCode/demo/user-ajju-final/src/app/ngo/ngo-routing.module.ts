import { NgoDetailsComponent } from './ngo-details/ngo-details.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgoListComponent } from './ngo-list/ngo-list.component';

const routes: Routes = [

  {
    path: 'ngo-list',
    component: NgoListComponent,
  },
  {
    path: 'ngo-details',
    component: NgoDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NgoRoutingModule { }
