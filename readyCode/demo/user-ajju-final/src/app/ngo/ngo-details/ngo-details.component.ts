import { NgoService } from './../ngo.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngo-details',
  templateUrl: './ngo-details.component.html',
  styleUrls: ['./ngo-details.component.css']
})
export class NgoDetailsComponent implements OnInit {

 
  ngo=null

  constructor(
    private router: Router,private ngoService : NgoService,
    private activatedRoute: ActivatedRoute
  ) { }
  ngOnInit(): void {

    
    const id = this.activatedRoute.snapshot.queryParams['id']
    if (id) {
      // edit product
      this.ngoService
        .getNgoDetails(id)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const ngos = response['data']
            if (ngos.length > 0) {
              
              this.ngo=ngos[0]
              console.log(this.ngo)
              // this.title=this.article['article_title']
              // this.description=this.article['article_description']
            }
          }
        })
    }
  }

  onUpdate(doctor) {
    this.router.navigate(['/home/doctor/doctor-update'], {queryParams: {id: doctor['id']}})
  }

  onBack(){
    this.router.navigate(['/home/ngo/ngo-list'])
  }

}
