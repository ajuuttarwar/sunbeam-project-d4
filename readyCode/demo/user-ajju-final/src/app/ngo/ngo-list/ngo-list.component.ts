import { Router } from '@angular/router';
import { NgoService } from './../ngo.service';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngo-list',
  templateUrl: './ngo-list.component.html',
  styleUrls: ['./ngo-list.component.css']
})

export class NgoListComponent implements OnInit {

  ngos = []

  constructor( private router: Router,private ngoService : NgoService) { }

  ngOnInit(): void {

    this.loadNgos()
  }


  loadNgos() {
    this.ngoService
      .getNgos()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.ngos = response['data']
        } else {
          console.log(response['error'])
        }
      })
  }


  onEdit(ngo) {
    this.router.navigate(['/home/ngo/ngo-details'], {queryParams: {id: ngo['ngo_id']}})
  }

}