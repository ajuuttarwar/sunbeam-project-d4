import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgoRoutingModule } from './ngo-routing.module';
import { NgoListComponent } from './ngo-list/ngo-list.component';
import { NgoDetailsComponent } from './ngo-details/ngo-details.component';


@NgModule({
  declarations: [NgoListComponent, NgoDetailsComponent],
  imports: [
    CommonModule,
    NgoRoutingModule
  ]
})
export class NgoModule { }
