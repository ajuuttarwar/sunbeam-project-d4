import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NgoService {

  
  url = 'http://localhost:5000/user/'

  constructor(private httpClient: HttpClient) { }

  getNgos() {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    
    return this.httpClient.get(this.url+ "/get-all-ngo", httpOptions)
  }


  getNgoDetails(id) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };
   
   return this.httpClient.get(this.url + "/get-ngo-by-id/" + id, httpOptions)
  }
 
}
