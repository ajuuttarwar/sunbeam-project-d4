import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  firstName=''
  lastName=''
  phone=''
  email = ''
  password = ''

  constructor(
    private toastr : ToastrService,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit(): void {
  }

  onSignup() {
    if(this.firstName.length == 0){
      this.toastr.error('please enter first Name')

    }else if(this.lastName.length == 0){
      this.toastr.error('please enter last Name')

    }else if(this.phone.length == 0){
      this.toastr.error('please enter phone Number')

    }
    else if(this.email.length == 0){
      this.toastr.error('please enter email')
    }else if(this.password.length == 0){
      this.toastr.error('please enter password')
    }else{
      this.authService.signup(this.firstName, this.lastName,this.phone, this.email, this.password)
    .subscribe(response => {
      if (response['status'] == 'success') {
        this.toastr.success(`You are Registered Successfully!! Please check your email!!`)
        this.router.navigate(['/auth/login'])
      } else {
        this.toastr.error('error while registering')
     
      }
    })
    }
    
  }
}
