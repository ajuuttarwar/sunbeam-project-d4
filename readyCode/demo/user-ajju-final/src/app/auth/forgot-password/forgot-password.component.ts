import { AuthService } from './../auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

 
  firstName = ''
  lastName = ''
  address = ''
  city = ''
  country = ''
  zip = ''
  phone = ''
  email = ''
  newEmail=''
  constructor(

    private toastr: ToastrService,

    private router : Router,
    private authService : AuthService,
    
  ) { }

  ngOnInit(): void {
  }

  onForgot(){
    this.authService.getProfile(this.newEmail).
    subscribe(response=>{
      console.log(response['data'])

      if(response['status'] == 'success'){
        const data= response['data']
        if(data != null){
          console.log(data)
          // this.firstName = data['firstName']
          // this.lastName = data['lastName']
          // this.address = data['address']
          // this.city = data['city']
          // this.country = data['country']
          // this.zip = data['zip']
          // this.phone = data['phone']
          this.email = data['email']

          console.log(this.email)

          if(this.newEmail.match(this.email) == null){
            this.toastr.error(`No user found`)
          }else{
            this.authService.sendOtp(this.email).
            subscribe(response=>{
              if(response['status'] == 'success'){
                console.log(response['data'])
                this.toastr.info(`OTP sent`)
                const data = response['data']
                const otp = data['otp']
                const em = data['email']
                this.router.navigate(['/auth/verify-otp'],{state:{otp : otp, email : em}})
              }else{
                this.toastr.error(`otp not sent`)
              }

            })

          }
        }else{
          console.log("not getting")
        }
        
      }else{
this.toastr.error(`Invalid email`)
      }
    })
  }


}
