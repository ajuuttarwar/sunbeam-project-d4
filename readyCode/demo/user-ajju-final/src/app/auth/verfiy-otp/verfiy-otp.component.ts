import { ToastrService } from 'ngx-toastr';
import { AuthService } from './../auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-verfiy-otp',
  templateUrl: './verfiy-otp.component.html',
  styleUrls: ['./verfiy-otp.component.css']
})
export class VerfiyOtpComponent implements OnInit {

  
  otp=''
  email =''
  
  newOtp=''
  password=''
  newPassword=''
  constructor(private router: Router,
    private authService: AuthService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.otp = history.state.otp
    this.email = history.state.email
  }

  // onReset() {
  //   if (this.password.match(this.newPassword) === null) {
  //     this.toastr.error(`Password Doesn't Match`)
  //   } else {
  //     this.authService.afterForgot(this.email, this.password)
  //     .subscribe(response => {
  //       if (response['status'] == 'success') {
  //         this.toastr.success(`Password updated successfully`)
  //         this.router.navigate(['/auth/login'])
  //       }
  //     })
        
  //   }

  // }

  onReset() {
    if (this.newOtp.length == 0) {
      this.toastr.error(`Please Enter OTP`)

    } else if (this.password.length == 0) {
      this.toastr.error(`Please Enter Password`)

    } else if (this.newPassword.length == 0) {
      this.toastr.error(`Please Re-enter Password`)

    } else if (this.otp !== this.newOtp) { 

      this.toastr.error(`Please Enter Correct OTP`)

    } else {

      if (this.password.match(this.newPassword) === null) {
        this.toastr.error(`Password Doesn't Match`)
      } else {
        this.authService.afterForgot(this.email, this.password)
          .subscribe(response => {
            if (response['status'] == 'success') {
              this.toastr.success(`Password updated successfully`)
              this.router.navigate(['/auth/login'])
            }
          })

      }
    }


  }

}
