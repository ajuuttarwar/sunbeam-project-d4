import { DoctorService } from './../doctor.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctor-update',
  templateUrl: './doctor-update.component.html',
  styleUrls: ['./doctor-update.component.css']
})
export class DoctorUpdateComponent implements OnInit {

  doctor=null
  firstName=''
  lastName=''
  specialist=''
  hospital_name=''
  hospital_id=0
  address=''
  city=''
  country=''
  zip=''
  phone=''
  email=''
  password=''

  constructor(
    private router: Router,private doctorService : DoctorService,
    private activatedRoute: ActivatedRoute
  ) { }
  ngOnInit(): void {

    
    const id = this.activatedRoute.snapshot.queryParams['id']
    if (id) {
      // edit product
      this.doctorService
        .getDoctorDetails(id)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const doctors = response['data']
            if (doctors.length > 0) {
              
              this.doctor=doctors[0]
              this.firstName=this.doctor['firstName']
              this.lastName=this.doctor['lastName']
              this.specialist=this.doctor['specialist']
              this.hospital_name=this.doctor['hospital_name']
              this.address=this.doctor['address']
              this.city=this.doctor['city']
              this.country=this.doctor['country']
              this.zip=this.doctor['zip']
              this.phone=this.doctor['phone']
              this.email=this.doctor['email']
              this.password=this.doctor['password']
              this.hospital_id=this.doctor['hospital_id']
              // console.log(this.doctor)
              // this.title=this.article['article_title']
              // this.description=this.article['article_description']
            }
          }
        })
    }
  }

  onUpdate(doctor){
    this.doctorService.updateDoctorDetails(this.doctor['id'],this.firstName,this.lastName,this.specialist,this.hospital_name,this.hospital_id,this.address,this.city,this.country,this.zip,this.phone,this.email,this.password)
    .subscribe(response => {
      if (response['status'] == 'success') {
        this.router.navigate(['/home/doctor/doctor-details/this.doctor.id'])
      }
    })

  }
}
