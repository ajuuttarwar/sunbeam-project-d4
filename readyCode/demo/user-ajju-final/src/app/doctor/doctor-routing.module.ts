import { AppointmentBookingComponent } from './appointment-booking/appointment-booking.component';
import { DoctorAppointmentComponent } from './doctor-appointment/doctor-appointment.component';
import { SearchDoctorComponent } from './search-doctor/search-doctor.component';
import { DoctorUpdateComponent } from './doctor-update/doctor-update.component';
import { DoctorDetailsComponent } from './doctor-details/doctor-details.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DoctorListComponent } from './doctor-list/doctor-list.component';

const routes: Routes = [

  {
    path: 'doctor-list',
    component: DoctorListComponent,
  },
  {
    path: 'doctor-details',
    component: DoctorDetailsComponent,
  },
  {
    path: 'doctor-update',
    component: DoctorUpdateComponent,
  },
  {
    path: 'search-doctor',
    component: SearchDoctorComponent,
  },
  {
    path: 'doctor-appointment',
    component: DoctorAppointmentComponent,
  },
  {
    path: 'appointment-booking',
    component: AppointmentBookingComponent,
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorRoutingModule { }
