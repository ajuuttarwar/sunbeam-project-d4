import { ToastrService } from 'ngx-toastr';
import { DoctorService } from './../doctor.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-doctor',
  templateUrl: './search-doctor.component.html',
  styleUrls: ['./search-doctor.component.css']
})
export class SearchDoctorComponent implements OnInit {
  doctors = []

  city=''
  specialist=''

tmpCity=[]

  searchCity=[]
  searchSpecialist=[]


  constructor(  private toastr : ToastrService,private router: Router,private doctorService : DoctorService,    private activatedRoute: ActivatedRoute) { }


  ngOnInit(): void {

    this.getCity()
    this.getSpecialist()

    const field = this.activatedRoute.snapshot.queryParams['specialist']

    if(field){

      this.doctorService
      .getDoctorByField(field)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.doctors = response['data']
          console.log(this.doctors)
        } else {
          console.log(response['error'])
        }
      })
    }
    else{
      this.doctorService
      .getDoctors()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.doctors = response['data']
          console.log(this.doctors)
        } else {
          console.log(response['error'])
        }
      })
     
    }
  
  
  }


getCity(){
  this.doctorService
      .getCity()
      .subscribe(response => {
        if (response['status'] == 'success') {
         this.tmpCity = response['data']
       
          // for (let index = 0; index < tmpCity.length; index++) {
          // //  console.log(tmpCity[index])
          // this.searchCity.push(tmpCity[index]['city'])
            
          // }
          console.log(this.tmpCity)
         
        } else {
          console.log(response['error'])
        }
      })
}

getSpecialist(){
  this.doctorService
      .getSpecialist()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.searchSpecialist = response['data']
          console.log(this.searchSpecialist)
         
        } else {
          console.log(response['error'])
        }
      })
}



  onSearch(){

    if(this.city.length==0){
      this.toastr.error("please enter city")
    }else if(this.specialist.length==0){
      this.toastr.error("please enter speciality")
    }else{


    this.doctorService.getDoctorByCitySpeciality(this.city,this.specialist)
    .subscribe(response => {
      if (response['status'] == 'success') {
        this.doctors = response['data']
        console.log(this.doctors)
        this.router.navigate(['/home/doctor/search-doctor'])

      } else {
        console.log(response['error'])
      }
    })
    }

  
  }


  onBook(doctor){
    console.log(doctor)
    this.router.navigate(['/home/doctor/doctor-details'], {queryParams: {id: doctor['id']}})
  }

}
