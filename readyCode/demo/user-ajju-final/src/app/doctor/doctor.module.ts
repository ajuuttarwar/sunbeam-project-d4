import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorRoutingModule } from './doctor-routing.module';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { DoctorDetailsComponent } from './doctor-details/doctor-details.component';
import { DoctorUpdateComponent } from './doctor-update/doctor-update.component';
import { SearchDoctorComponent } from './search-doctor/search-doctor.component';
import { DoctorAppointmentComponent } from './doctor-appointment/doctor-appointment.component';
import { AppointmentBookingComponent } from './appointment-booking/appointment-booking.component';


@NgModule({
  declarations: [DoctorListComponent, DoctorDetailsComponent, DoctorUpdateComponent, SearchDoctorComponent, DoctorAppointmentComponent, AppointmentBookingComponent],
  imports: [
    CommonModule,
    DoctorRoutingModule,
    FormsModule
  ]
})
export class DoctorModule { }
