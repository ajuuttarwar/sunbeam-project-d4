import { Router } from '@angular/router';
import { DoctorService } from './../doctor.service';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css']
})


export class DoctorListComponent implements OnInit {

  doctors = []

  constructor( private router: Router,private doctorService : DoctorService) { }

  ngOnInit(): void {
    this.loadDoctors()
  }
  loadDoctors() {
    this.doctorService
      .getDoctors()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.doctors = response['data']
        } else {
          console.log(response['error'])
        }
      })
  }

  onEdit(doctor) {
    this.router.navigate(['/home/doctor/doctor-details'], {queryParams: {id: doctor['id']}})
  }

  onSearch(){

    this.router.navigate(['/home/doctor/search-doctor'])
  }



}



