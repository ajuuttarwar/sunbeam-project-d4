import { ToastrService } from 'ngx-toastr';
import { UserService } from "./../../user/user.service";
import { DoctorService } from "./../doctor.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-doctor-appointment",
  templateUrl: "./doctor-appointment.component.html",
  styleUrls: ["./doctor-appointment.component.css"],
})
export class DoctorAppointmentComponent implements OnInit {
  name = "";
  phone = "";
  email = "";
  date = "";
  password = "";
  user = null;
  constructor(
    private toastr : ToastrService,
    private router: Router,
    private doctorService: DoctorService,
    private userService: UserService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.userService.getUserDetails().subscribe((response) => {
      if (response["status"] == "success") {
        this.user = response["data"];
        this.name = this.user["firstName"];
        this.email = this.user["email"];
        this.phone = this.user["phone"];
        //  console.log(this.user)
      }
    });
  }

  onBack() {
    this.router.navigate(["/home/doctor/doctor-list"]);
  }

  onConformAppointment() {
    const id = this.activatedRoute.snapshot.queryParams["id"];

    if(this.phone==""){
      this.toastr.error('enter your phone number ')
    }else if(this.date==""){
      this.toastr.error('enter date')
    }else{
    this.doctorService
    .BookAppointment(id, this.name, this.email, this.phone, this.date)
    .subscribe((response) => {
      if (response["status"] == "success") {
        this.toastr.success('Appointment Conform')
        this.router.navigate(["/home/doctor/appointment-booking"]);
      }else{
        // console.log("fialed appointment");
      }
    });
    }

  }
}
