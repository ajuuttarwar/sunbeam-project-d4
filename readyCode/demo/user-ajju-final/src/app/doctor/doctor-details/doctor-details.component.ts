import { DoctorService } from './../doctor.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctor-details',
  templateUrl: './doctor-details.component.html',
  styleUrls: ['./doctor-details.component.css']
})
export class DoctorDetailsComponent implements OnInit {

  doctor=null

  constructor(
    private router: Router,private doctorService : DoctorService,
    private activatedRoute: ActivatedRoute
  ) { }
  ngOnInit(): void {

    
    const id = this.activatedRoute.snapshot.queryParams['id']
    if (id) {
      // edit product
      this.doctorService
        .getDoctorDetails(id)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const doctors = response['data']
            if (doctors.length > 0) {
              
              this.doctor=doctors[0]
              console.log(this.doctor)
              // this.title=this.article['article_title']
              // this.description=this.article['article_description']
            }
          }
        })
    }
  }

  onUpdate(doctor) {
    this.router.navigate(['/home/doctor/doctor-update'], {queryParams: {id: doctor['id']}})
  }

  onBack(){
    this.router.navigate(['/home/doctor/doctor-list'])
  }

  onSearch(doctor){
    this.router.navigate(['/home/doctor/doctor-appointment'], {queryParams: {id: doctor['id']}})
  }
}
