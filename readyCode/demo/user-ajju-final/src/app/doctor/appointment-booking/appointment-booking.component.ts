import { ToastrService } from 'ngx-toastr';
import { UserService } from './../../user/user.service';
import { DoctorService } from './../doctor.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-appointment-booking',
  templateUrl: './appointment-booking.component.html',
  styleUrls: ['./appointment-booking.component.css']
})
export class AppointmentBookingComponent implements OnInit {


  bookings = [];
  constructor(
    private toastr : ToastrService,
    private router: Router,
    private doctorService: DoctorService,
    private userService: UserService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.doctorService.getDoctorAppointmentDetails().subscribe((response) => {
      if (response["status"] == "success") {
        this.bookings = response["data"];
        console.log(this.bookings)
        //  console.log(this.user)
      }
    });
  }

  


  onCancel(booking){
    const id=booking['id']
    this.doctorService.deleteDoctorAppointment(id).subscribe((response) => {
      if (response["status"] == "success") {
       
        console.log('success')
        this.toastr.success(`appointment deleted successfully`)
        this.ngOnInit()
        //  console.log(this.user)
      }
    });

  }

  onBook(){
    this.router.navigate(["/home/doctor/search-doctor"]);

  }

}
