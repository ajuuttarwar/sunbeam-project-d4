import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  url = 'http://localhost:5000/user'


  constructor(private httpClient: HttpClient) { }


  getCity() {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + "/getCity", httpOptions)
  }

  getSpecialist() {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + "/getSpecialist", httpOptions)
  }




  getDoctors() {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + "/get-all-doctors", httpOptions)
  }

  getDoctorDetails(id) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })

   };
   
   return this.httpClient.get(this.url + "/get-doctor-by-id/" + id, httpOptions)
  }
 
  getDoctorByField(field) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
       
     })
     
   };

   const body={specialist : field}
   
   return this.httpClient.post(this.url + "/get-doctor-by-field",body, httpOptions)
  }

  // firstName=''
  // lastName=''
  // specialist=''
  // hospital_name=''
  // address=''
  // city=''
  // country=''
  // zip=''
  // phone=''
  // email=''
  // password=''
  updateDoctorDetails(id,firstName: string,lastName: string,specialist: string,hospital_name: string,hospital_id:number,address: string,city: string,country: string,zip: string,phone: string,email: string,password: string){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body={
      firstName: firstName,
  lastName: lastName,
  specialist: specialist,
  hospital_name: hospital_name,
  hospital_id:hospital_id,
  address: address,
  city: city,
  country: country,
  zip: zip,
  phone: phone,
  email: email,
  password: password
    }

    return this.httpClient.put(this.url + "/edit-doctor/" + id, body, httpOptions)
  }

  getDoctorByCitySpeciality(city: string , specialist: string){

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
        
      })
      
    };
 
    const body={city:city,specialist:specialist}
    
    return this.httpClient.post(this.url + "/get-doctor-by-city-specialist",body, httpOptions)
  }




  BookAppointment(id,name: string ,email: string ,phone: string ,date: string ){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    console.log(sessionStorage['token'])
    const body={
   name:name,
   email:email,
   phone:phone,
   date:date
    }

    return this.httpClient.post(this.url + "/book-appointment/" + id,body, httpOptions)

  }



  getDoctorAppointmentDetails(){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + "/getDoctorAppointmentDetails", httpOptions)
  }


  deleteDoctorAppointment(id){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    // http://localhost:5000/user/deleteDoctorAppointment/2


    return this.httpClient.delete(this.url + "/deleteDoctorAppointment/" + id, httpOptions)


  }

}
