import { ArticleService } from './../article.service';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {

  articles = []
  

  constructor(
    private router: Router,
    private articleService: ArticleService) { }

  ngOnInit(): void {
    this.loadArticles()
  }

  loadArticles() {
    this.articleService
      .getArticles()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.articles = response['data']
          console.log(this.articles)
        } else {
          console.log(response['error'])
        }
      })
  }
}

