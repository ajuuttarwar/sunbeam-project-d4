import { ToastrService } from 'ngx-toastr';
import { UserService } from './../user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user=null

  selectedFile = null

  image=''
  constructor(
    private router: Router,private userService : UserService,
    private activatedRoute: ActivatedRoute,
    private toastr : ToastrService
  ) { }
 

  ngOnInit(): void {
    
      this.userService
        .getUserDetails()
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.user = response['data']
            this.image=this.user['image']
          //  console.log(this.user)
          }
        })
    
  }


  onEdit(user) {
    console.log(user)
    this.router.navigate(['/home/user/edit-profile'], {queryParams: {id: user['id']}})
  }


  uploadImage() {
    this.router.navigate(['/home/user/upload-image'])
  }



  onImageSelect(event) {
    this.selectedFile = event.target.files[0]
  }


  onUploadImage() {
    // const id = this.activatedRoute.snapshot.queryParams['id']
    const id=sessionStorage['id']


    if(this.selectedFile == null){
      this.toastr.error('please select file')
    }else{

      this.userService
      .uploadImage(this.selectedFile)
      .subscribe(response => {
        if (response['status'] == 'success') {
          // this.router.navigate(['/home/user/profile'])
          this.ngOnInit()
          
        } else {
          console.log(response['error'])
        }
      })
    }
    
  }


}
