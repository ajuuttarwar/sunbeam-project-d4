import { ToastrService } from 'ngx-toastr';
import { UserService } from './../user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.css']
})
export class UploadImageComponent implements OnInit {

  selectedFile = null
  
  constructor(
    private router: Router,
    private toastr : ToastrService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService) { }

  ngOnInit(): void {
  }

  onImageSelect(event) {
    this.selectedFile = event.target.files[0]
  }


  onUploadImage() {
    const id = this.activatedRoute.snapshot.queryParams['id']


    if(this.selectedFile == null){
      this.toastr.error('please select file')
    }else{

      this.userService
      .uploadImage(this.selectedFile)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.router.navigate(['/home/user/edit-profile'])
        } else {
          console.log(response['error'])
        }
      })
    }
    
  }

}
