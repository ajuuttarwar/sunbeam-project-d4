import { ToastrService } from 'ngx-toastr';
import { UserService } from './../user.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  password=''

  newPassword=''

  constructor(private router: Router,
    private userService: UserService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
  }




  resetPassword() {
    if(this.password.length == 0) {
      this.toastr.error('Please enter password!') 
    } else if(this.newPassword.length == 0) {
      this.toastr.error('Please confirm password!')
    } else if(this.password !== this.newPassword) {
      this.toastr.error('Password not matching!')
    } else {
      console.log(this.password)
      console.log(this.newPassword)
      this.userService
      .resetPassword(this.password)
      .subscribe(response => {
          if (response['status'] == 'success') {
            this.toastr.success('Password changed successfully!')
            this.router.navigate(['/auth/login'])
          } else {
            this.toastr.error('Something went wrong! Please try again!')
          }
      })
    }
  }
 
  // resetPassword() {
  //   if (this.password.match(this.newPassword) === null) {
  //     this.toastr.error(`Password Doesn't Match`)
  //   } else {
  //     this.userService
  //       .resetPassword(this.password)
  //       .subscribe(response => {
  //         if (response['status'] == 'success') {


  //           sessionStorage.removeItem('token')
  //           sessionStorage.removeItem('firstName')
  //           sessionStorage.removeItem('lastName')


  //           this.router.navigate(['/auth/login'])
  //         }
  //       })
  //   }

  // }

}
