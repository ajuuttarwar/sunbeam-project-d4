import { UserService } from './../user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

firstName=''
lastName=''
city=''
country=''
zip=''
phone=''
email=''
image=''

user=null

constructor(
  private router:Router,
  private userService:UserService,
  private activatedRoute: ActivatedRoute
) { }

ngOnInit(): void 
  {
 
    const id = this.activatedRoute.snapshot.queryParams.id
   
    
    if (id) {
      // edit product
      this.userService
        .getUserDetails()
        .subscribe(response => {
          if (response['status'] == 'success') {
            const user = response['data']
            console.log(user['image'])  

            // this.id= this.ngo['ngo_id']
              this.firstName = user['firstName']
              this.lastName = user['lastName']

              this.city = user['city']
              this.country = user['country']
              this.zip = user['zip']
              this.phone = user['phone']
              this.email = user['email']
              
              
            }
        })
  }
  }



  onEdit(){
    this.userService
        .updateUserProfile( this.firstName,this.lastName,this.city,this.country,this.zip, this.phone, this.email)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/home/user/profile'])
          }
        })
  }

  uploadImage() {
    this.router.navigate(['/home/user/upload-image'])
  }


}
