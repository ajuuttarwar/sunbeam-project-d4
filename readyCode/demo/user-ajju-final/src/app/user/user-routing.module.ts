import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UploadImageComponent } from './upload-image/upload-image.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [

  {
    path: 'profile',
    component: ProfileComponent,
  }, 
  
  {
    path: 'edit-profile',
    component: EditProfileComponent,
  },
  {path : 'upload-image' , component : UploadImageComponent},
  {path : 'reset-password' , component : ResetPasswordComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
