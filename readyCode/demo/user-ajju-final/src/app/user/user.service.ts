import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = 'http://localhost:5000/user'


  constructor(private httpClient: HttpClient) { }
  getUserDetails() {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })

   };
   
   return this.httpClient.get(this.url + "/profile" , httpOptions)
  }
 

  updateUserProfile(
    firstName: string,
    lastName: string,
    city: string,
    country: string,
    zip: string,
    phone: string,
    email: string
  ) {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token'],
      }),
    };

    const body = {
      firstName: firstName,
      lastName: lastName,
      city: city,
      country: country,
      zip: zip,
      phone: phone,
      email: email,
  
    };

    return this.httpClient.put(this.url + "/edit-user" , body, httpOptions);
  }



  uploadImage(file) {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = new FormData()
    body.append('image', file)

    return this.httpClient.post(this.url + `/upload-image`, body, httpOptions)
  }



  resetPassword(password: string) {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    
    const body = {
      password: password
    }

    return this.httpClient.put(this.url + "/reset-password", body, httpOptions)
  }







  // getNgoDetails(id) {
  
  
  //   // add the token in the request header
  //   const httpOptions = {
  //     headers: new HttpHeaders({
  //       token: sessionStorage['token'],
  //     }),
  //   };

  //   return this.httpClient.get(this.url + '/' + id, httpOptions);
  // }

}
