**Admin**
-----------------------------------------------------------------------------------
**A)ADMIN sign-in**(view part)

1)Doctor (Button):edit
	-list of all the doctors
	-search with attributes(search box)
	-FILTERS:(dropdown search box with intellisense suggestion)
		*City wise
		*speciality wise
	-SORT BY:(DROPBOX)
		*Alphabetical Order
		*Distance(using in future with maps api)
	
2)Hospital (Button):
	-list of all the hospitals (cards)
	-search with hospital name/speciality(search box)
	-FILTERS:(search box)
		*City wise
		*speciality wise
	-SORT BY:(DROPBOX selection)
		*Alphabetical Order
		*Distance(using in future with maps api)


3)NGO (Button):
	-List of all the NGO (cards)
	-Search with NGO attributes(search box)
	-FILTERS:(search box)
		*City wise
		*type wise
	-SORT BY:(DROPBOX)
		*Alphabetical Order
		*Distance(using in future with maps api)

4)Articles (Button):
	-List of all the articles(default-sorted by latest first)(CARDS)
		-Filter: CATAGORIES Buttons(Select Buttons)
			*All
			*Lifestyle
			*YOGA
			*Nutrition
			*Ayurveda
			*Technology & Innovations


-----------------------------------------------------------------------------------------------
##B)ADMIN PROFILE MANAGEMENT section:

	*FIRST NAME-(disabled text view)
	*LAST NAME-(disabled text view)
	*EMAIL-(disabled text view)
	*CONTACT NO.-(disabled text view)
	
		--edit profile(button)-will enable text view.(toggle-submit)
	

	*change password(button):
		-old password()
		-new password
		-enter new password again
				$atleast @one uppercase,@one lowercase,@number,@special character required..!						

		 *reset password(button)
		 *clear(button)

		 *back to profile(button)

------------------------------------------------------------------------------------------------------------------------



****Management section****


#user management
   -list all the users
   -add new user
   -delete user
   -update user		

#doctor management
   -list all the doctors
   -add new doctor
   -delete doctor
   -update doctor

#hospital management
   -list all the hospital
   -add new hospital
   -delete hospital
   -update hospital

#ngo management
   -list all the ngo
   -add new ngo
   -delete ngo
   -update ngo

#article management
   -list all the article%
   -add new article%
   -delete article
   -update article









